<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
const web = 'Web\\';
const Customerprofile = 'Customerprofile\\';

Route::namespace(web)->group(function()
{

    Route::get('/','HomeController@index');



});



Route::get('/about',function(){

    return view('front_end.web.about');

});

Route::get('/digitizing',function(){

    return view('front_end.web.digitizing');

});

Route::get('/vector',function(){

    return view('front_end.web.vector');

});
Route::get('/patches',function(){

    return view('front_end.web.patches');

});

Route::get('/contact',function(){

    return view('front_end.web.contact');

});


Route::get('/freedownloadsignup',function(){

    return view('front_end.web.signup');

});





 Route::get('patches_Quote',function(){


    return view('front_end.profile.pateches.patches_quote');

 });



 Route::get('myprofile',function(){


   return view('front_end.profile.tool.myprofile');

});





 Route::get('myprofile',function(){


   return view('front_end.profile.tool.myprofile');

});




                // Customerprofile route

Route::namespace(Customerprofile)->group(function(){

    Route::group(['middleware' => ['auth']], function () {

                         //  Digitizing
            Route::get('/Digitizing/View','DigitizingController@index');
            Route::get('Digitizing/edit/{id}','DigitizingController@edit')->name('digitizing.edit');
            Route::post('Digitizing/update/{id}','DigitizingController@update')->name('digitizing.update');
            Route::get('/Digitizing/Record/{id}','DigitizingController@show')->name('digitizing_view.show');
            Route::get('/Profile','DigitizingController@create');
            Route::post('/Digitizing/store','DigitizingController@store')->name('digitizing.store');

                   //Digitizing Quote
             Route::get('/Digitizing/Quote','DigitizingQuoteController@index');
             Route::get('/Digitizing/Quote/edit/{id}','DigitizingQuoteController@edit')->name('digitizing.qoute.edit');
             Route::post('/Digitizing/Quote/update/{id}','DigitizingQuoteController@update')->name('digitizing.qoute.update');
             Route::get('/Digitizing/Quote/Record/{id}','DigitizingQuoteController@show')->name('digitizing.qoute.show');
             Route::get('Digitizing/Quote/Create','DigitizingQuoteController@create')->name('digitizing.quote.create');
             Route::post('/Digitizing/Quote/store','DigitizingQuoteController@store')->name('digitizing.quote.store');


               //  Vector
               Route::get('Vector/View','VectorController@index');
               Route::get('Vector/edit/{id}','VectorController@edit')->name('vector.edit');
               Route::post('Vector/update/{id}','VectorController@update')->name('vector.update');
               Route::get('Vector/Record/{id}','VectorController@show')->name('vector.show');
               Route::get('Vector/add','VectorController@create')->name('vector.create');
               Route::post('Vector/store','VectorController@store')->name('vector.store');


               //   Vector Quote
               Route::get('Vector/Quote','VectorquoteController@index');
               Route::get('Vector/Show/{id}','VectorquoteController@show')->name('vector.show');
               Route::get('Vector/Quote/edit/{id}','VectorquoteController@edit')->name('vector.quote.edit');
               Route::post('Vector/Quote/update/{id}','VectorquoteController@update')->name('vector.quote.update');
               Route::get('Vector/Quote/Add','VectorquoteController@create')->name('vector.create');
               Route::post('Vector/Quote/store','VectorquoteController@store')->name('vector.store');



               // Patches
               Route::get('Patches/View','PatchesController@index');
               Route::get('Patches/Show/{id}','PatchesController@show')->name('patches.show');
               Route::get('Patches/edit/{id}','PatchesController@edit')->name('Patches.edit');
               Route::post('Patches/update/{id}','PatchesController@update')->name('Patches.update');
               Route::get('Patches/Add','PatchesController@create')->name('patches.create');
               Route::post('Patches/store','PatchesController@store')->name('patches.store');


               //  Patches Quote
               Route::get('Patches/Quote','PatchesQuoteController@index');
               Route::get('Patches/Quote/Show/{id}','PatchesQuoteController@show')->name('Patches.quote.show');
               Route::get('Patches/Quote/Edit/{id}','PatchesQuoteController@edit')->name('Patches.quote.edit');
               Route::post('Patches/Quote/Update/{id}','PatchesQuoteController@update')->name('Patches.quote.update');
               Route::get('Patches/Quote/Add','PatchesQuoteController@create');
               Route::post('Patches/Quote/Store','PatchesQuoteController@store')->name('patches.quote.store');




                    //  Tool section

                    // change password
                Route::get('change/password','ChangepasswordController@create')->name('password.create');
                Route::post('change/password/store','ChangepasswordController@store')->name('password.store');

                // MyProfile

                Route::get('Bussiness/Information','BussinessController@index');
                Route::post('Bussiness/Information/update','BussinessController@update')->name('bussiness.update');
                Route::post('Payment/update','PaymentController@update')->name('payment.update');
                Route::post('Payment/store','PaymentController@store')->name('payment.store');


                Route::get('padding/order','PendingorderController@index');


 //
});



               // signup route
                Route::get('/signup','SignupController@signup')->name('signup.create');
                Route::post('/signup/store','SignupController@store')->name('signup.store');



               // Login
               Route::get('/login','LoginController@create')->name('login');
               Route::post('/login/action','LoginController@postlogin')->name('login.action');
               Route::get('/logout','LoginController@logout');

            });




         Route::get('cache', function() {
            Artisan::call('cache:clear');
            Artisan::call('view:clear');
            Artisan::call('route:clear');
            Artisan::call('config:clear');
           return 'cache clear';
        });
