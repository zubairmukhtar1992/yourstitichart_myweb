<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;
class SignupeMail extends Mailable
{
    public $email;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->email = $request;

        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Wellcome To Yourstitichart')
            ->from($this->email->email, $this->email->name)
            ->cc($this->email)
            ->to(config('yourstitchart.mail_to'))
            ->view('signup-email.signup_email');
    }
}
