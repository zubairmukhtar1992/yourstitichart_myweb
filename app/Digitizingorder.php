<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Digitizingorder extends Model
{

     protected $table="digitizing_orders";
     protected $casts = ['image' => 'array'];

      protected $fillable =['id','order_name','height','width','urgent','image','order_placement','required_format','order_fabric','instruction','user_id'];


     public function user()
     {

         return $this->belongsTo('App\User');
     }


}
