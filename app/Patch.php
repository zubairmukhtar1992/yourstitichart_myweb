<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patch extends Model
{
     
    protected $table = "Patches_orders";

    protected $fillable =['id','Width','Height','image','Embroidered','Received_date','Iron','Quantity','Date','Instruction','user_id'];
    


}
