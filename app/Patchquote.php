<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patchquote extends Model
{
      protected $fillable =['id','width','height','image','Embroidered','Received_date','released_date','Iron','Quantity','Date','Instruction','user_id'];

    protected $table ="patches_quote";



    public function user()
    {

        return $this->belongsTo('App\User');
    }



}


