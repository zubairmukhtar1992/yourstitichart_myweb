<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VectorQuote extends Model
{
    
    protected $table ="vector_quote";

    protected $fillable =['id','quote_name','instruction','image','user_id'];

    
    public function user()
    {

        return $this->belongsTo('App\User');
    }



}
