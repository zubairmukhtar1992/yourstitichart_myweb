<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vectororder extends Model
{

    protected $table ="vector_orders";

    protected $fillable =['id','vector_name','height','width','image','color','order','order_fabric','instruction','user_id'];


    public function user()
    {

        return $this->belongsTo('App\User');
    }

}
