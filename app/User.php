<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    protected $table ="users";

    
    public function digitizing()
    {
    
        return $this->hasMany('App\Digitizingorder','user_id');
    }


    public function digitizingquote()
    {
    
        return $this->hasMany('App\Digitizingqoute','user_id');
    }

    public function vector()
    {
    
        return $this->hasMany('App\Vectororder','user_id');
    }
    public function vectorquote()
    {
    
        return $this->hasMany('App\VectorQuote','user_id');
    }

      
    public function Patch()
    {
    
        return $this->hasMany('App\Patch','user_id');
    }


    public function Patchquote()
    {
    
        return $this->hasMany('App\Patchquote','user_id');
    }
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
