<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Digitizingqoute extends Model
{
    
       protected $table="digitizing_quote";

       protected $fillable =['quote_name','image','height','width','received_date','quote_placement','order_fabric','instruction','user_id'];


       public function user()
       {
   
           return $this->belongsTo('App\User');
       }



}
