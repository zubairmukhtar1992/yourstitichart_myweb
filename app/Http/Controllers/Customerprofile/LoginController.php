<?php

namespace App\Http\Controllers\Customerprofile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Validator, Redirect, Response, Session;
use Carbon\Carbon;
class LoginController extends Controller
{

      public function create()
      {

            return view('front_end.profile.login');
      }


      public function postlogin(Request $request)
      {

            $this->validate(
                  $request,
                  [
                        'username' =>  'required',
                        'password' =>   'required'

                  ]
            );

            if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
                  $userStatus = Auth::User()->status;
                  Auth::user()->last_sigin_in_at = Carbon::now()->toDateTimeString();
                  $clientIP = \Request::getClientIp(true);
                  Auth::user()->last_login_ip = $clientIP;
                  Auth::user()->save();
                  if ($userStatus == '1') {
                        return redirect()->intended(url('/Profile'));
                  } else {
                        Auth::logout();
                        Session::flush();
                        return redirect(url('login'))->withInput()->with('message', 'You are temporary blocked. please contact to admin');
                  }
            } else {

                  return redirect(url('login'))->withInput()->with('message', 'Incorrect username or password. Please try again.');
            }
      }

      public function logout()
      {
            Session::flush();
            Auth::logout();
            return Redirect('/login');
      }
}
