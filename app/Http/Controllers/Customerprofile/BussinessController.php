<?php

namespace App\Http\Controllers\Customerprofile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage; 
use File;
class BussinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[

             'bussiness'=>Auth::user()

        ];
           

        return view('front_end.profile.tool.Bussinessprofile',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        
         
        $first_name     =   $request->first_name;
        $last_name      =   $request->last_name;
        $company_name   =   $request->company_name;
        $phone          =   $request->phone;
        $Country        =   $request->Country;
       
        $user =Auth::user()->id;

        if($request->hasFile('image'))
        {
         $image = $request->file('image');
      
        $extension = $image->getClientOriginalExtension();
        
        $content = new User();
        $content->image = $image->getFilename() . '.' . $extension;
    
       $check=User::where('id',$user)->update(['first_name'=>$first_name,'last_name'=>$last_name,
       'company_name'=> $company_name,'phone'=>$phone,'image'=> $content->image]);
       
        }
        else    
        {
            $check=User::where('id',$user)->update(['first_name'=>$first_name,'last_name'=>$last_name,
            'company_name'=> $company_name,'phone'=>$phone]);          
        }
        
            return back()->withSuccess('Profile Updated !',$check);
    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
