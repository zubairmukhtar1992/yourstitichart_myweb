<?php

namespace App\Http\Controllers\Customerprofile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use Alert;
use Auth;
use App\Pendingorder;
use App\Digitizingorder;
use App\Vectororder;
class PendingorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      

        $data=[
                 'Pendingorders'=>Pendingorder::get(),       
                 'Digitizingorders'=>Auth::user()->digitizing()->orderByDesc("created_at")->get(),
                 'Vectororders'=>Auth::user()->vector()->orderByDesc("created_at")->get(),
              ];

     
       
        return view('front_end.profile.tool.pendingorders',$data);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
