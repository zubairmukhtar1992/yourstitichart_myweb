<?php

namespace App\Http\Controllers\Customerprofile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Digitizingqoute;
Use Alert;
use Auth;
use File;
use App\User;
use Illuminate\Support\Facades\Storage;
class DigitizingQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          
        $data=

        [
          'digitizingquotes'=>Auth::user()->digitizingquote()->paginate(3)
        ];
    
      
        return view('front_end.profile.digitizing.Digitizing_Quote_View',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front_end.profile.digitizing.Digitizing_Quote');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
            $this->validate($request,[


                  'quote_name'=>'required',
                  'height' =>'required',
                  'width' => 'required',
                  'quote_placement'=>'required',
                  'order_fabric'=>'required',
                  'instruction'=>'required',
                  'image' =>'required'
                  ],
                 ['quote_name.required'       => 'Quote name is requird',
                 'height.required'           => 'Height is requird',
                 'width.required'            => 'Width is requird',
                 'quote_placement.required'  => 'Quote placement is requird',
                 'order_fabric.required'     => 'Order fabric is requird',
                 'instruction.required'      => 'Instruction is requird',
                 'image.required'            => 'Artwork  is requird',
                ]
                );
  
            
                $qoute_name          =    $request->quote_name;
                $height              =    $request->height;
                $width               =    $request->width;
                $quote_placement     =    $request->quote_placement;
                $order_fabric        =    $request->order_fabric;
                $instruction         =    $request->instruction;
                $user_id             =    Auth::user()->id;
            
          
               $image = $request->file('image');


               $extension = $image->getClientOriginalExtension();
               Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
               $content = new Digitizingqoute;
               $content->image = $image->getFilename() . '.' . $extension;
               $check = Digitizingqoute::where('id', $content->id)
               ->select('image')->create(['quote_name' => $qoute_name ,'height'=>$height,'width'=>$width,'quote_placement'=>$quote_placement,
               'order_fabric'=>$order_fabric,
               'instruction'=>$instruction,'user_id'=> $user_id,'image'=>$content->image])->get();
               return back()->withSuccess('SUCCESSFULLY CREATED');

     
    }

   /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
               $data=[

              'digitizing_qoutes'=>Digitizingqoute::findOrFail($id)
              
              ];

        return view('front_end.profile.digitizing.Digitizing_Quote_Record',$data);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $data=[
           
             'digitizingqoute_edit'=>Digitizingqoute::findOrFail($id)

        ];

        return view('front_end.profile.digitizing.Digitizing_Quote_edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

             $this->validate($request,[


                  'quote_name'=>'required',
                  'height' =>'required',
                  'width' => 'required',
                  'quote_placement'=>'required',
                  'order_fabric'=>'required',
                  'instruction'=>'required',
                  'image' =>'required'
                  ],
                 ['quote_name.required'       => 'Quote name is requird',
                 'height.required'           => 'Height is requird',
                 'width.required'            => 'Width is requird',
                 'quote_placement.required'  => 'Quote placement is requird',
                 'order_fabric.required'     => 'Order fabric is requird',
                 'instruction.required'      => 'Instruction is requird',
                 'image.required'            => 'Artwork  is requird',
                ]
                );
  
            
                $qoute_name          =    $request->quote_name;
                $height              =    $request->height;
                $width               =    $request->width;
                $quote_placement     =    $request->quote_placement;
                $order_fabric        =    $request->order_fabric;
                $instruction         =    $request->instruction;
                $user_id             =    Auth::user()->id;
            
                if($image=$request->hasfile('image'))
                {
           
               $image = $request->file('image');


               $extension = $image->getClientOriginalExtension();
               Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
               $content = new Digitizingqoute;
               $content->image = $image->getFilename() . '.' . $extension;

               Digitizingqoute::where('id',$id)->update(['quote_name' => $qoute_name ,'height'=>$height,'width'=>$width,'quote_placement'=>$quote_placement,
               'order_fabric'=>$order_fabric,
               'instruction'=>$instruction,'user_id'=> $user_id,'image'=>$content->image]);
                       
            }
            else{
                Digitizingqoute::where('id',$id)->update(['quote_name' => $qoute_name ,'height'=>$height,'width'=>$width,'quote_placement'=>$quote_placement,
                'order_fabric'=>$order_fabric,
                'instruction'=>$instruction,'user_id'=> $user_id]);
                
            }
              
              
              
               return back()->withSuccess('SUCCESSFULLY UPDATED');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
