<?php

namespace App\Http\Controllers\Customerprofile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use  App\Digitizingorder;
use App\User;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DigitizingController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $data =

      [
        'digitizings' => Auth::user()->digitizing()->paginate(8)
      ];


    return view('front_end.profile.digitizing.digitizing_view_order', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {

    return view('front_end.profile.digitizing.digitizing_place_order');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // return $request;


    $this->validate(
      $request,
      [

        'order_name' => 'required',
        'height' => 'required',
        'width' => 'required',
        'order_placement' => 'required',
        'required_format' => 'required',
        'order_fabric' => 'required',
        'instruction' => 'required',
        'image' => 'required'

      ],
      [
        'order_name.required'                  => 'Order name is required',
        'height.required'                      => 'Height is required',
        'width.required'                       => 'Width is required',
        'order_placement.required'             => 'Order Placement is required',
        'required_format.required'             => 'Format is required',
        'order_fabric.required'                => 'Order fabric is required',
        'instruction.required'                 => 'instruction is required',
        'image.required'                       => 'Artwork is required'

      ]
    );
    $order_name             = $request->order_name;
    $height                 = $request->height;
    $width                  = $request->width;
    $order_placement        = $request->order_placement;
    $required_format        = $request->required_format;
    $order_fabric           = $request->order_fabric;
    $instruction            = $request->instruction;
    $urgent                 = $request->customRadioInline1;
    $user_id = Auth::user()->id;
    $image = [];
    // return $request->file('image');
    foreach ($request->file('image') as $key => $value) {
      $extension = $value->getClientOriginalExtension();
      $image[$key] = $value->getFilename() . '.' . $extension;

      Storage::disk('yourstitchart')->put($value->getFilename() . '.' . $extension, File::get($value));
    }

    $content = new Digitizingorder;
    $content->image =  $image;

    Digitizingorder::where('id', $content->id)
      ->select('image')->create([
        'order_name' => $order_name, 'height' => $height, 'width' => $width, 'order_placement' => $order_placement,
        'required_format' => $required_format, 'order_fabric' => $order_fabric,
        'instruction' => $instruction, 'user_id' => $user_id, 'urgent' => $urgent, 'image' => $content->image
      ])->get();




    return true;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = [

      'digitizing_view' => Digitizingorder::findOrFail($id)

    ];

    return view('front_end.profile.digitizing.Digitizing_Record', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {

    $data =
      [

        'digitizing_edit' => Digitizingorder::findOrFail($id)

      ];

    return view('front_end.profile.digitizing.edit', $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {

    $this->validate($request, [

      'order_name' => 'required',
      'height' => 'required',
      'width' => 'required',
      'order_placement' => 'required',
      'required_format' => 'required',
      'order_fabric' => 'required',
      'instruction' => 'required',

    ],);

    $order_name             = $request->get('order_name');
    $height                 = $request->get('height');
    $width                  = $request->get('width');
    $order_placement        = $request->get('order_placement');
    $required_format        = $request->get('required_format');
    $order_fabric           = $request->get('order_fabric');
    $instruction           = $request->get('instruction');
    $user_id = Auth::user()->id;

    if ($image = $request->hasfile('image')) {
      $image = $request->file('image');

      $extension = $image->getClientOriginalExtension();
      Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
      $content = new Digitizingorder;
      $content->image = $image->getFilename() . '.' . $extension;

      Digitizingorder::where('id', $id)->update([
        'order_name' => $order_name, 'height' => $height, 'width' => $width, 'order_placement' => $order_placement,
        'required_format' => $required_format, 'order_fabric' => $order_fabric,
        'instruction' => $instruction, 'user_id' => $user_id, 'image' => $content->image
      ]);
    } else {

      Digitizingorder::where('id', $id)->update([
        'order_name' => $order_name, 'height' => $height, 'width' => $width, 'order_placement' => $order_placement,
        'required_format' => $required_format, 'order_fabric' => $order_fabric,
        'instruction' => $instruction, 'user_id' => $user_id
      ]);
    }
    return back()->withSuccess('SUCCESSFULLY UPDATED !');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
