<?php

namespace App\Http\Controllers\Customerprofile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Alert;
use App\Mail\SignupeMail;
use Illuminate\Support\Facades\Hash;
use Mail;

class SignupController extends Controller
{

    public function signup()
    {
        return view('front_end.profile.signup');
    }

    public function store(REQUEST $request)
    {

        $this->validate(
            $request,
            [

                'username' => 'required|unique:users',
                'current_password' => 'required',
                'confirm_password' => 'required',
                'confirm_password' => ['required', 'same:current_password'],
                'first_name' => 'required',
                'last_name' => 'required',
                'company_name' => 'required',
                'phone' => 'required|min:11|numeric',
                'email' => 'required|unique:users|max:255',
                'Country' => 'required'

            ],
            [

                'username.required'           =>   'Username is required',
                'current_password.required'   =>   'Password is required',
                'first_name.required'         =>   'First name is required',
                'last_name.required'          =>   'Last name is required',
                'company_name.required'       =>   'Company name is required',
                'phone.required'              =>   'Phone number is required',
                'email.required'              =>   'Email is required',
                'Country.required'            =>   'Country name is required',
                'confirm_password.required'   =>   'Confirm Password is required'

            ]


        );


        $user                  =   new User;
        $user->first_name     =   $request->first_name;
        $user->last_name      =   $request->last_name;
        $user->username       =   $request->username;
        $user->company_name   =   $request->company_name;
        $user->phone          =   $request->phone;
        $user->Country        =   $request->Country;
        $user->email          =   $request->email;
        $user->password       =   Hash::make($request->current_password);
        $user->remember_token  = $request->get('_token');

        Mail::send(new SignupeMail($request));

        $user->save();

        return redirect('/Profile');
    }
}
