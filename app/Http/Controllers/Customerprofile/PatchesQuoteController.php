<?php

namespace App\Http\Controllers\Customerprofile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Alert;
use Auth;
use App\User;
use File;
use App\Patchquote;
use Illuminate\Support\Facades\Storage;
class PatchesQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data=[
   
              
               'patchquotes'=>Auth::user()->Patchquote()->paginate(3)

              

             ];



        return view('front_end.profile.pateches.pateches_quote_view',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('front_end.profile.pateches.patches_quote');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[

            'width'=>'required',
            'height'=>'required',
            'embroidered'=>'required',
            'iron'=>'required',
            'quantity'=>'required',
            'date'=>'required',
            'instruction'=>'required',
            'image'=>'required'
         
           ],
          [
              'width.required'                  => 'Width is required',
              'height.required'                 => 'Height is required',
              'embroidered.required'            => 'Embroidered is required',
              'iron.required'                   => 'Iron on / Heat Seal is required',
              'quantity.required'               => 'Quantity is required',
              'date.required'                   => 'Date is required',
              'instruction.required'            => 'Instruction is required',
               'image.required'                 => 'Artwork is required'               

           ]
         );

               $width          =   $request->width;
               $height         =   $request->height;
               $embroidered    =   $request->embroidered;
               $iron           =   $request->iron;
               $quantity       =   $request->quantity;
               $date           =   $request->date;
               $instruction    =   $request->instruction;
               $user_id =Auth::user()->id;


               $image = $request->file('image');

               $extension = $image->getClientOriginalExtension();
               Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
               $content = new Patchquote;
               $content->image = $image->getFilename() . '.' . $extension;

               $check = Patchquote::where('id', $content->id)
              ->select('image')->create(['width'=>$width,'height'=>$height,'Embroidered'=>$embroidered,
              'Iron'=>$iron,'Quantity'=>$quantity,'Date'=>$date,
              'Instruction'=>$instruction,'user_id'=>$user_id,'image'=>$content->image]);

        return back()->withSuccess('SUCCESSFULLY CREATED');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function show($id)
       {
            $data=[

                
            'patchequoterecord'=>Patchquote::findOrFail($id)


             ];

             return view('front_end.profile.pateches.patches_quote_record',$data);
         }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[

                
            'patchequoteedit'=>Patchquote::findOrFail($id)


             ];

             return view('front_end.profile.pateches.edit_quote',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $width          =   $request->width;
        $height         =   $request->height;
        $embroidered    =   $request->embroidered;
        $iron           =   $request->iron;
        $quantity       =   $request->quantity;
        $date           =   $request->date;
        $instruction    =   $request->instruction;
        $user_id =Auth::user()->id;

         if($image=$request->hasfile('image'))
         {
          $image = $request->file('image');

          $extension = $image->getClientOriginalExtension();
          Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
          $content = new Patchquote;
          $content->image = $image->getFilename() . '.' . $extension;

          $check = Patchquote::where('id',$id)
          ->select('image')->update(['width'=>$width,'height'=>$height,'Embroidered'=>$embroidered,
          'Iron'=>$iron,'Quantity'=>$quantity,'Date'=>$date,
          'Instruction'=>$instruction,'user_id'=>$user_id,'image'=>$content->image]);
           }
           else{

            $check = Patchquote::where('id',$id)
            ->select('image')->update(['width'=>$width,'height'=>$height,'Embroidered'=>$embroidered,
            'Iron'=>$iron,'Quantity'=>$quantity,'Date'=>$date,
            'Instruction'=>$instruction,'user_id'=>$user_id]);
           }
       return back()->withSuccess('SUCCESSFULLY UPDATED!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
