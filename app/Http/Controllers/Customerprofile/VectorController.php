<?php

namespace App\Http\Controllers\Customerprofile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use Alert;
use Auth;
use File;
use App\User;
use App\Vectororder;
use Illuminate\Support\Facades\Storage;
class VectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $data=
          [
              'vectororders'=>Auth::user()->vector()->paginate(3)

          ];


        return view('front_end.profile.vector.Vector_View_Order',$data);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('front_end.profile.vector.Vector_place_order');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        return $request;
            



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=
        [
             
            'vectorview'=>Vectororder::findOrFail($id)

        ];


        return view('front_end.profile.vector.Vector_Record_Order',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[
             
        'vectoredits'=>Vectororder::findOrFail($id)

           ];
           return view('front_end.profile.vector.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[


            'vector_name'=>'required',
            'height' =>'required',
            'width' => 'required',
            'color'=>'required',
            'order'=>'required',
            'order_fabric'=>'required',
            'instruction'=>'required',
            'image' =>'required'
            ],
           ['vector_name.required'       => 'Vector name is required',
           'height.required'             => 'Height is required',
           'width.required'              => 'Width is required',
           'color.required'              => 'Color type is required',
           'order.required'              => 'Order is required',
           'order_fabric.required'       => 'Order fabric is required',
           'instruction.required'        => 'Instruction is required',
           'image.required'              => 'Artwork  is required',
          ]
          );



          $vector_name          =    $request->vector_name;
          $height               =    $request->height;
          $width                =    $request->width;
          $color                =    $request->color;
          $order                =    $request->order;
          $order_fabric         =    $request->order_fabric;
          $instruction          =    $request->instruction;
          $user_id              =    Auth::user()->id;
      
    
         $image = $request->file('image');


         $extension = $image->getClientOriginalExtension();
         Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
         $content = new Vectororder;
         $content->image = $image->getFilename() . '.' . $extension;
         $check = Vectororder::where('id', $id)->update(['vector_name' => $vector_name ,'height'=>$height,'width'=>$width,'color'=>$color,
         'order'=>$order,'order_fabric'=>$order_fabric,
         'instruction'=>$instruction,'user_id'=> $user_id,'image'=>$content->image]);
         return back()->withSuccess('   SUCCESSFULLY UPDATED');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
