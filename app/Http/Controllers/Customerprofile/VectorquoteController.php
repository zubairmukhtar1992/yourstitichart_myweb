<?php

namespace App\Http\Controllers\Customerprofile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use Alert;
use Auth;
use App\User;
use App\VectorQuote;
use File;
use Illuminate\Support\Facades\Storage;
class VectorquoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
          $data =[
  
          'vectorquotes'=>Auth::user()->vectorquote()->paginate(4)
 
             ];
        


        return view('front_end.profile.vector.vector_view_quote',$data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('front_end.profile.vector.vector_quote');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
         $this->validate($request,[

             'quote_name'=>'required',
             'instruction'=>'required',
             'image' =>'required'
         
            ],

            [
                'quote_name.required'         => 'Quote name is required',
                'instruction.required'        => 'Instruction is required',
                'image.required'              => 'Artwork is required'
             ]);

            
            $quote_name     =  $request->quote_name;
            $instruction    =  $request->instruction;
            $image = $request->file('image');


            $user=Auth::user()->id;
          
            $extension = $image->getClientOriginalExtension();
            Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
            $content = new VectorQuote();
            $content->image = $image->getFilename() . '.' . $extension;

             $check = VectorQuote::where('id', $user)->create(['quote_name'=>$quote_name,'instruction'=>$instruction,'image'=> $content->image,'user_id'=>$user]);
              

              return back()->withsuccess('Succussfully Save');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=[
          'vectorquote'=>VectorQuote::findorFail($id)
             
        ];


         return view('front_end.profile.vector.Vector_Record_Order',$data);
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $data=[
   
               'editquote'=>VectorQuote::findorFail($id)

           ];

           return view('front_end.profile.vector.editquote',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

           $quote_name     =  $request->get('quote_name');
           $instruction    =  $request->get('instruction');
           $user=Auth::user()->id;


            if($image=$request->hasfile('image')){
           $image = $request->file('image');

         
           $extension = $image->getClientOriginalExtension();
           Storage::disk('yourstitchart')->put($image->getFilename() . '.' . $extension, File::get($image));
           $content = new VectorQuote();
           $content->image = $image->getFilename() . '.' . $extension;

            VectorQuote::where('id',$id)->update(['quote_name'=>$quote_name,'instruction'=>$instruction,'image'=> $content->image,'user_id'=>$user]);
        }
        else
        {

             VectorQuote::where('id',$id)->update(['quote_name'=>$quote_name,'instruction'=>$instruction,'user_id'=>$user]);

        }

             return back()->withsuccess('Succussfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
