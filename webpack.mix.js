const mix = require('laravel-mix');


mix.browserSync('http://localhost/yourstitichart/public/')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.ts('resources/ts/app.ts', 'public/js').extract(['vue'])
    .sass('resources/sass/app.scss', 'public/css');

    if (mix.inProduction()) {
        mix.version();
    }
