<?php

return [
  'file_url'  => env('FILE_URL'),
  'mail_to'  => env('MALE_TO'),
];
