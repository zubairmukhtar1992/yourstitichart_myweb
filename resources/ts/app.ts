require('./bootstrap');

import Vue from "vue"
import "../sass/app.scss"


import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);
import VFormView from './components/form/index.vue';
// Vue.component("file-uploader", FileUploadView)
Vue.component("v-form", VFormView)


new Vue({
  el: "#wrapper",
})
