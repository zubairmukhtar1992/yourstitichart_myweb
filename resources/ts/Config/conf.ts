
let url: HTMLMetaElement | null = document.head.querySelector(
    'meta[name="app-url"]'
);
let imgurl: HTMLMetaElement | null = document.head.querySelector(
    'meta[name="image-url"]'
);
const AppUrl = url?.content
const ImageUrl = imgurl?.content
export { AppUrl, ImageUrl }
