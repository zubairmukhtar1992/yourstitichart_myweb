import Master from '../master/main';
import { Component, Prop } from 'vue-property-decorator';
import FileUploadView from '../fileuploader/index.vue';
import Axios from 'axios';

@Component({
  components: {
    FileUploadView
  }
})
export default class VForm extends Master {
  @Prop() url: string
  validated = false
  Mopen: boolean = false
  name = ""
  height = ""
  weight = ""
  oredr = ''
  format = ""
  fabric = ""
  Instruction = ""
  urgent = ""
  Images: Array<File> = []
  get valid() {
    if (this.name && this.height && this.weight && this.oredr && this.format && this.fabric
      && this.Instruction && this.urgent && this.Images.length
    ) {
      return true
    }
    return false
  }
  Mclose() {
    this.Mopen = false
  }

  async submit() {
    this.validated = true
    if (this.valid) {
      let form = new FormData();
      form.append('order_name', this.name);
      form.append('height', this.height)
      form.append('width', this.weight)
      form.append('order_placement', this.oredr)
      form.append('required_format', this.format)
      form.append('order_fabric', this.fabric)
      form.append('instruction', this.Instruction)
      form.append('customRadioInline1', this.urgent)
      this.Images.forEach((item, i) => {
        console.log(item)
        form.append(`image[${i}]`, item)
      })
      // form.append('image',this.Images)

      let res = await Axios.post(this.url, form);
      if (res.data == 1) {
        this.$swal('Order Placed', 'Your Order has been placed and will be reviewed shortly', 'success')
      }

    }
  }
  setImages(images: Array<File>) {
    this.Images = images
    this.Mopen = false
  }

}
