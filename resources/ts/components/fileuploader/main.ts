import { Component, Vue, Prop } from 'vue-property-decorator';
import Master from '../master/main';
import Modal from '../Modal/index.vue';
import Axios from 'axios';


@Component({
  components: {
    Modal
  }
})
export default class FileUploaderCompnent extends Master {
  @Prop() open:boolean
  @Prop() url!: string
  form: any = ""
  objectView = []
  SelectedImage=0
  Images:Array<File> = []

  get Image() {
    if(this.objectView.length){
      return this.objectView[this.SelectedImage]
    }
    return this.AppUrl+'/images/'+ "placeholder-image.png"
  }

  humanFileSize(size:any) {
    let i = Math.floor( Math.log(size) / Math.log(1024) );
    //@ts-ignore
    return (( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i]);
};
  logImages(event: any) {
    // for (let items of this.objectView) {
    //   URL.revokeObjectURL(items);
    // }
    // this.objectView = []
    // this.Images = []
    for (let items of event.target.files) {
      this.Images.push(items)
      this.objectView.push(URL.createObjectURL(items))
    }


  }
  close(){
this.$emit('close')
  }
  async save() {
    this.$emit('Images',this.Images)
  }
  mounted() {
    //@ts-ignore
    // const element:HTMLFormElement = document.querySelector('#v-form')!;
    // this.form =new FormData(element);

    // element.addEventListener('submit', event => {
    //   event.preventDefault();
    // });
  }
}
