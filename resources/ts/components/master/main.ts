import {Vue} from "vue-property-decorator"
import { AppUrl ,ImageUrl} from "../../Config/conf"
export default class Master extends Vue{
    get AppUrl(){
        return AppUrl
    }
    get FileUrl(){
        return ImageUrl
    }
}
