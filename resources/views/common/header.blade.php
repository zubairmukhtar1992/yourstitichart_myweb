<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Yourstitchart</title>
	<link type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,600|Open+Sans:400,600" rel="stylesheet" />
	<link type="text/css" href="{{ url('') }}/assets/files/css/style.css" rel="stylesheet" />
	<link type="text/css" href="{{ url('') }}/assets/files/css/responsive.css" rel="stylesheet" />
	<link type="text/css" href="{{ url('') }}/assets/files/css/design.css" rel="stylesheet" />
	<link rel="stylesheet" href="{{ url('') }}/assets/files/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ url('') }}/assets/files/css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="{{ url('') }}/assets/files/css/owl.theme.default.min.css">
  <link href="{{ url('') }}/css/app.css" rel="stylesheet" type="text/css" />


</head>
<style>

li.list {
    float: left;
    padding-left: 13px;
    position: relative;
    left: -14px;
    padding-top: 13px;
}
.sep {
    float: left;
    width: 100%;
    height: 1px;
    background: #d1cfd285;
    position: relative;
}
hr {
    margin-top: 4px;
    margin-bottom: 4px;
    border: 0;
    border-top: 1px solid #7070708c;
}
.links>li>a {
    list-style-type: none;
    color: white;
    text-decoration: none;
}
.footer-column-1.col-md-6.col-lg-6.col-sm-12 {
    position: relative;
    top: -13px;
}
.heading {
    font-family: 'Raleway', sans-serif;
    color: white;
    padding-top: 6px;
    text-align: justify;
}
.footercontent {
    padding-top: 18px;
    padding-left: 29px;
}
.footerlogo{
	width: 52%;
    margin-left: 16px;
    margin-top: 20px;
}
.footer-content {
    float: left;
    background-color: black;
	width: 100%;
	height:100%;

}
.column-inner {
    width: 100%;
    background-color: #1a1a1a;
	height:90%;
	border-top: 13px solid #fad65a;
}

    .product{
	background: url("../images/product_bg.jpg");
	min-height: 98px;
	width: 100%;
	margin: 30px 0 25px;
	-webkit-background-size: cover;
	background-size: cover;
	float: left;
}
	.owl-theme .owl-nav {
    display: none;
}
.owl-theme .owl-dots, .owl-theme .owl-nav {
    text-align: center;
    -webkit-tap-highlight-color: transparent;
    margin-top: 19px;
}
.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
    background: #000000;
}
.error
{
	color:red;
}

</style>
<body  >
	<div class="header_top">
		<div class="container">
			<br>
			<div class="customer_login"> <a href="{{url('/signup')}}" style="color:white;text-decoration:none;"   id="ctl00_customer_sign">
                Sign Up
            </a>
			</div>
			<div class="customer_login" style="margin-right:10px"> <a  style="color:white;text-decoration:none;"   href="{{url('/login')}}"  class="fancybox" >
				Log In
            </a>
			</div>
			<!-- navigation -->
			<div id="nav" class="navigation">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					</li>
					<li><a href="{{url('/about')}}">About</a>

					</li>
					<li>
					<a href="{{url('/digitizing')}}">Digitizing</a>

					</li>

			     	<li>
			     	<a href="{{url('/vector')}}">Vector</a>

                    </li>
                    <li>
                    <a href="{{url('/patches')}}" >Patches</a>

					</li>
					</li>
					<li>
					<a href="{{url('/login')}}">Free Downloads</a>
					</li>

					<li>
					<a href="{{url('/')}}"> Promotion</a>
					</li>
					<li class="last">
					<a  href="{{url('/contact')}}">Contact Us</a>
					</li>

				</ul>
			</div>


		</div>

	</div>
  <!-- Modal
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content" id="modelbody">
		<div class="modal-header" id="modelheader">
		  <h5 class="modal-title" id="exampleModalLongTitle"><span style="color:white;"><center>LOG INN</center></span></h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		<div class="row">
		 <div class="col-lg-12">
		<div class="form-group">
		    <label>Username</label>
        <input type="text" name="username" class="form-control" placeholder="Enter Username" required>
		</div>
		</div>
		<div class="col-lg-12">
		<div class="form-group">
		<label>Password</label>
        <input type="text" name="Password" class="form-control" placeholder="Enter Password" required>
		</div>
		</div>
		</div>
		</div>
		<div class="modal-footer" id="modelfooter">
		    <a href="{{url('/Profile')}}" class="btn btn-link"  value="Log Inn" id="signinleft" >Login In</a>


		</div>
	  </div>
	</div>
  </div> -->

	<!-- navigation -->
	<div class="header_btm">
		<div class="container">

			<!-- logo -->
			<div class="logo">
				<img src="{{ url('') }}/assets/uploads/yourstitchart/logo/logo.png" alt="logo" style="
                    width: 92%;margin-left: 9px;  margin-top: 8px;" />
			</div>
			<!-- logo -->
			<div class="top_taqline">We provide sew out with every design</div>
		</div>
		<!--end container-->
           <div class="mobile-nav">
			<div id="main">
           <button class="openbtn" onclick="openNav()">☰ </button>
           </div>

          <div id="mySidebar" class="sidebar">
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>

				<a href="{{url('/')}}">Home</a>

					<a href="{{url('/about')}}">About</a>

					<a href="{{url('/digitizing')}}">Digitizing</a>


			     	<a href="{{url('/vector')}}">Vector</a>

                    <a href="{{url('/')}}" target="_blank">Patches</a>


						<a href="{{url('/')}}">Free Downloads</a>

						<a href="{{url('/')}}"> Promotion</a>

					<a href="{{url('/contact')}}">Contact Us</a>


				</ul>
				<ul>
<li class="mobile_customer closenav">
                                    <div class="c-login">
                                      <a style="color:white;text-decoration:none;" data-toggle="modal" data-target="#exampleModalCenter" href="javascript:void(0);"  class="fancybox" data-fancybox-type="iframe">
			                          	Log Inn
                                                 </a>

                                    </div>
                                    <div class="c-login">
                                       <a href="{{url('/signup')}}" style="color:white;text-decoration:none;"   id="ctl00_customer_sign">
                                       Sign Up
                                        </a>
                                    </div>
                                </li>
				</ul>

          </div>
      </div>
        </div>
	<!-- header -->
