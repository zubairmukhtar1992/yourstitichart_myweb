<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title> YourStitchart Customer Profile</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="" name="" />
  <meta content="Coderthemes" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- App favicon -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="app-url" content="{{ url("") }}">
  <meta name="image-url" content=" {{ config('yourstitchart.file_url') }}">


  <!-- App css -->
  <link href="{{ url('') }}/assets/files/dist/css/bootstrap.min.css" rel="stylesheet"
    type="text/css" />
  <link href="{{ url('') }}/assets/files/dist/css/icons.min.css" rel="stylesheet" type="text/css" />
  <link href="{{ url('') }}/assets/files/dist/css/app.min.css" rel="stylesheet" type="text/css" />
  <link href="{{ url('') }}/css/app.css" rel="stylesheet" type="text/css" />

</head>
<style>
  .nav-pills>li>a,
  .nav-tabs>li>a {
    color: #020202;
    font-weight: 600;
  }

  .navtab-bg li>a {
    background-color: #e3e3ec;
    margin: 0 5px;
  }

  .nav-pills .nav-link.active,
  .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #fad65a;
  }

  .card-box {
    background-color: #ffffff;
    padding: 1.5rem;
    -webkit-box-shadow: 0 0.75rem 6rem rgba(56, 65, 74, .03);
    box-shadow: 0 0.75rem 6rem rgba(56, 65, 74, .03);
    margin-bottom: 24px;
    border-radius: .25rem;
  }

  .navbar-custom {
    background-color: #020202;
    -webkit-box-shadow: 0 0 35px 0 rgba(154, 161, 171, .15);
    box-shadow: 0 0 35px 0 rgba(154, 161, 171, .15);
    padding: 0 10px 0 0;
    position: fixed;
    left: 0;
    right: 0;
    height: 70px;
    z-index: 100;
  }

  body {
    height: 100%;

    width: 100%;
    background-image: linear-gradient(180deg, #dfdee98f, #fad65a85);
  }

  hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid #e3e3ec;
  }

  .card-header {
    padding: 1rem 1.5rem;
    margin-bottom: 0;
    background-color: #FAD65A;
    border-bottom: 0 solid #f7f7f7;
    color: white;
  }

  a:hover {
    color: #fad65a;
    text-decoration: none;
  }

  .card {
    margin-bottom: 24px;
    -webkit-box-shadow: 0 0.75rem 6rem rgba(56, 65, 74, .03);
    box-shadow: 0 0.75rem 6rem rgba(56, 65, 74, .03);
    box-shadow: 2px 2px 6px 0 rgba(0, 0, 0, .16);
  }

  .card-body {
    /* -webkit-box-flex: 1; */
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 21px;
    padding-left: 9px;
    line-height: 6px;

  }

  a {
    color: #020202;
    font-size: 16px;
    text-decoration: none;
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
  }

  h3 {
    margin: -8px -17px;
    color: white;
  }

  .tools {
    background-color: red;
  }

  .error {
    position: relative;
    top: 15px;
    color: red;
  }

  .nav-user img {
    height: 50px;
    width: 50PX;
  }

</style>

<body>
  <!-- Begin page -->
  <div id="wrapper">

    <!-- Topbar Start -->
    <div class="navbar-custom">

      <ul class="list-unstyled topnav-menu float-right mb-0">

        <li class="dropdown notification-list">
          <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="false" aria-expanded="false">
            @if(Auth::user()->image)
              <img src="{{ Storage::disk('yourstitchart')->url(Auth::user()->image) }}"
                alt="user-image" class="rounded-circle ">
            @else
              @php
                $name = explode(" ",Auth::user()->username);
                $count = 1;
              @endphp
              <div style="background-color:white;
                      border-radius: 50px;
                      width: 50px;
                      height: 50px;
                      text-align: center;
                      padding-bottom: 0px !important;
                      line-height: 52px;
                      font-weight:800;
                      margin-top: 9px;
                      font-size: 24px;
                      color:#e74c3c;">@foreach ($name as $item){{ strtoupper($item[0] ?? null) }}@php
                $count++; if($count>2) break; @endphp@endforeach</div>
            @endif

          </a>
          <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
            <!-- item-->
            <div class="dropdown-header noti-title">
              <h6 class="text-overflow m-0">
                <h4 class="text-overflow m-0"> Hi, {{ ucfirst(Auth()->user()->username) }}</h4>
              </h6>
            </div>


            <!-- item-->
            <a href="{{ url('Bussiness/Information') }}" class="dropdown-item notify-item">
              <i class="fe-settings"></i>
              <span>My Profile</span>
            </a>



            <div class="dropdown-divider"></div>

            <!-- item-->
            <a href="{{ url('/logout') }}" class="dropdown-item notify-item">
              <i class="fe-log-out"></i>
              <span>Logout</span>
            </a>

          </div>
        </li>



      </ul>

      <!-- LOGO -->
      <div class="logo-box">
        <a href="{{ url('/Profile') }}" class="logo text-center">
          <span class="logo-lg">
            <img src="{{ url('/') }}/assets/files/dist/images/logo-light.png" alt="" height="18"
              style="
    height: 50px;">
            <!-- <span class="logo-lg-text-light">UBold</span> -->
          </span>

        </a>
      </div>


    </div>
    <!-- end Topbar -->



    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->



    <!-- Start Content-->
    <div class="container-fluid">
      <br>
      <br>
      <br>
      <!-- start page title -->
      <div class="row">
        <div class="col-12">
          <div class="page-title-box">
            <div class="page-title-right">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                <li class="breadcrumb-item active">Profile</li>
              </ol>
            </div>
            <h4 class="page-title">Dashboard </h4>
          </div>
        </div>
      </div>
      <!-- end page title -->


      <div class="row">
        <div class="col-xl-3">
          <div class="  card">
            <div class=" card-header ">
              <h3><i class="fas fa-align-center"></i> Digitizing Section</h3>
            </div>
            <div class="card-body">

              <a href="{{ url('/Profile') }}"><i class="mdi mdi-arrow-right"></i> Place Order</a>
              <hr>
              <a href="{{ url('/Digitizing/View') }}"><i class="mdi mdi-arrow-right"></i> View
                Orders Record</a>
              <hr>
              <a href="{{ url('/Digitizing/Quote/Create') }}"><i
                  class="mdi mdi-arrow-right"></i> Get Quote </a>
              <hr>
              <a href="{{ url('/Digitizing/Quote') }}"><i class="mdi mdi-arrow-right"></i> View
                Quotes Record</a>
              <br>
            </div>
          </div>
        </div>
        <div class="col-xl-3">
          <div class="card">
            <div class="card-header">
              <h3><i class="dripicons-pencil"></i> Vector Section</h3>
            </div>
            <div class="card-body">

              <a href="{{ url('Vector/add') }}"><i class="mdi mdi-arrow-right"></i> Place Order</a>
              <hr>
              <a href="{{ url('Vector/View') }}"><i class="mdi mdi-arrow-right"></i> View Orders
                Record</a>
              <hr>
              <a href="{{ url('Vector/Quote/Add') }}"><i class="mdi mdi-arrow-right"></i> Get
                Quote </a>
              <hr>
              <a href="{{ url('Vector/Quote') }}"><i class="mdi mdi-arrow-right"></i> View Quotes
                Record</a>
              <br>


            </div>
          </div>
        </div>
        <div class="col-xl-3">
          <div class="card">
            <div class="card-header">
              <h3><i class="mdi mdi-bookmark-outline"></i> Patches Section</h3>
            </div>
            <div class="card-body">
              <a href="{{ url('Patches/Add') }}"><i class="mdi mdi-arrow-right"></i> Place Order</a>
              <hr>
              <a href="{{ url('Patches/View') }}"><i class="mdi mdi-arrow-right"></i> View Orders
                Record</a>
              <hr>
              <a href="{{ url('Patches/Quote/Add') }}"><i class="mdi mdi-arrow-right"></i> Get
                Quote</a>
              <hr>
              <a href="{{ url('Patches/Quote') }}"><i class="mdi mdi-arrow-right"></i> View Quotes
                Record</a>
              <br>

            </div>
          </div>
        </div>
        <div class="col-xl-3">
          <div style="    border: 1px solid red;" class="card">
            <div class="card-header tools">
              <h3><i class="fas fa-cogs"></i> Tools</h3>
            </div>
            <div class="card-body">

              <a href="{{ url('padding/order') }}"><i class="mdi mdi-arrow-right"></i> Pending
                Orders</a>
              <hr>
              <a href="#"><i class="mdi mdi-arrow-right"></i> Invoices</a>
              <hr>
              <a href="{{ url('Bussiness/Information') }}"><i class="mdi mdi-arrow-right"></i> My
                Profile</a>
              <hr>
              <a href="{{ url('change/password') }}"><i class="mdi mdi-arrow-right"></i> Change
                Password</a>
              <br>

            </div>
          </div>
        </div>
      </div>
