 
 @include('sweetalert::alert')

 <div class=" container-fluid footer-content"  >
 <div class="container-fluid">
<div class="row">
 <div class="footer-column-1 col-md-6 col-lg-6 col-sm-12">

  <div class="column-inner">
  
				<!-- <img class="footerlogo"  src="{{ url('') }}/assets/uploads/yourstitchart/logo/logo.png" alt="logo"  /> -->
        <div class="footercontent">
        <div class="col-md-12 col-lg-12">
		    <h5 class="heading">ABOUT US</h5>
        <div class="sep"></div>
        <br>
        <p class="heading"  >We now sell worldwide and have won over 47 awards in various digitizing categories. Our team of 300+ members, all strive to be your favorite choice for digitizing and vector graphics.</p>
        
        <h5 class="heading">GET IN TOUCH</h5>
        <div class="sep"></div>

        <ul>
         <li class="list"><a href="https://www.facebook.com/" target="_blank"><img width="28px" src="{{url('')}}/assets/uploads/yourstitchart/icons/facebook.svg"</a></li>
         <li class="list"><a href="https://www.whatsapp.com/" target="_blank"><img width="28px" src="{{url('')}}/assets/uploads/yourstitchart/icons/watsapp.svg"</a></li>
         <li class="list"><a href="https://twitter.com/" target="_blank"><img width="28px" src="{{url('')}}/assets/uploads/yourstitchart/icons/twitter.svg"</a></li>
         <li class="list"><a href="https://www.youtube.com/" target="_blank"><img width="28px" src="{{url('')}}/assets/uploads/yourstitchart/icons/youtube.svg"</a></li>
         <li class="list"><a href="#" target="_blank"><img width="28px" src="{{url('')}}/assets/uploads/yourstitchart/icons/instragram.svg"</a></li>
        </ul>
        </div>
      </div>  

  </div>
 </div>   
 <div class="col-md-3 col-lg-3">
      <div class="footercontent">
      <h5 class="heading">NAVIGATION LINKS</h5>
      <div class="sep"></div>
      <br>
      <ul class="links">
      
					<li><a href="{{url('/')}}">Home</a>
					</li>
          <hr>
					<li><a href="{{url('/about')}}">About</a>
					</li>
          <hr>
					<li>
					<a href="{{url('/digitizing')}}">Digitizing</a>
					</li>
          <hr>
			    <li>
			    <a href="{{url('/vector')}}">Vector</a>
          </li>
          <hr>
           <li>
           <a href="{{url('/')}}" target="_blank">Patches</a>
					</li>
          <hr>
				
         
					<li>
					<a href="{{url('/')}}"> Promotion</a>
					</li>
          <hr>
					<li>
					<a  href="{{url('/contact')}}">Contact Us</a>
					</li>	
				</ul>
      </div>  <!-- footercontent  -->
</div> 
<div class="col-md-3 col-lg-3">
      <div class="footercontent">
      <h5 class="heading">CALL NOW</h5>
      <div class="sep"></div>
      <br>
      <ul class="links">
					<li><a href=""><i class="fa fa-mobile" aria-hidden="true"></i>03322858659</a>
					</li>
					<li><a href="">03322858659</a>
					</li>
          <ul>
          <br>
      <h5 class="heading">EMAIL NOW</h5>
      <div class="sep"></div>
      <br>
      <ul class="links">
					<li><a href="">Info@yourstitchart.com</a>
					</li>
					<li><a href="">Support@yourstitchart.com</a>
					</li>
          <ul>
      </div>  <!-- footercontent  -->
</div> 
</div>
</div><!--row-->

</div><!--end container-fluid-->


 <script>
function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

<!-- jQuery library -->
<script src="{{ url('') }}/assets/files/js/jquery.min.js"></script>

<script src="{{ url('') }}/assets/files/js/vue-modal.min.js"></script>

<!-- Popper JS -->
<script src="{{ url('') }}/assets/files/js/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="{{ url('') }}/assets/files/js/bootstrap.min.js"></script>

<script  src="{{ url('') }}/assets/files/js/jquer.js"></script>
<script  src="{{ url('') }}/assets/files/js/jquery-3.4.1.min.js"></script>

<script  src="{{ url('') }}/assets/files/js/owl.carousel.min.js"></script>


</body>

</html>