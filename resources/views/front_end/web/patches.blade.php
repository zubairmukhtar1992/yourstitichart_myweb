@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('')}}/assets/files/css/zoomifyc.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js">
</script>
<script type="text/javascript" src="{{url('')}}/assets/files/js/zoomifyc.js">
</script>
<style>
  .card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgb(250 214 90);
    border-bottom: 1px solid rgb(255 255 255);
    color: white;
    text-align: center;
    font-size: 31px;
  }
  #imgBox img {
    padding: 13px;
    width: 400px;
    height: 400px;
    position: relative;
    right: 39px;
  }
</style>
<div  class="bg-patches">
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
      </div>
      <div class="col-lg-5">
        <div class="card mt-5">
          <div class="card-header">
            Get Quote Now
          </div>
          <div class="card-body">
            <form>
              <div class="form-group row">
                <div class="col">
                  <label for="ex1">Width
                  </label>
                  <input type="text" class="form-control"  placeholder="Width">
                </div>
                <div class="col">
                  <label for="ex2">Height 
                  </label>
                  <input type="text" class="form-control"  placeholder="Height">
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <label for="ex1">Embroidered Patches
                  </label>
                  <select name="input_3" id="input_1_3" class="form-control" aria-required="true" aria-invalid="false">
                    <option value="Embroidered Patches">Embroidered Patches
                    </option>
                    <option value="Chenille Patches">Chenille Patches
                    </option>
                    <option value="Leather Patches">Leather Patches
                    </option>
                    <option value="PVC / Rubber Patches">PVC / Rubber Patches
                    </option>
                    <option value="Printed / Sublimated Patches">Printed / Sublimated Patches
                    </option>
                    <option value="Woven Patches">Woven Patches
                    </option>
                    <option value="Embroidered Keychains">Embroidered Keychains
                    </option>
                    <option value="Chenille Keychains">Chenille Keychains
                    </option>
                    <option value="Leather Keychains">Leather Keychains
                    </option>
                    <option value="PVC / Rubber Keychains">PVC / Rubber Keychains
                    </option>
                    <option value="Printed / Sublimated Keychains">Printed / Sublimated Keychains
                    </option>
                    <option value="Woven Keychains">Woven Keychains
                    </option>
                  </select>                  
                </div>
                <div class="col">
                  <label for="ex2">Iron on / Heat Seal
                  </label>
                  <select name="input_4" id="input_1_4" class="form-control" aria-required="true" aria-invalid="false">
                    <option value="Iron on / Heat Seal">Iron on / Heat Seal
                    </option>
                    <option value="Velcro (Both Hook &amp; Loop)">Velcro (Both Hook &amp; Loop)
                    </option>
                    <option value="Peel &amp; Stick / Self Adhesive">Peel &amp; Stick / Self Adhesive
                    </option>
                    <option value="Sew on">Sew on
                    </option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <label for="ex1">Quantity
                  </label>
                  <input type="number" class="form-control"  placeholder="Quantity">
                </div>
                <div class="col">
                  <label for="ex2">Date 
                  </label>
                  <input type="date" class="form-control"  placeholder="Date Required">
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <label for="ex1">Name
                  </label>
                  <input type="text" class="form-control"  placeholder="Name">
                </div>
                <div class="col">
                  <label for="ex2">Email 
                  </label>
                  <input type="email" class="form-control"  placeholder="Email">
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <label for="ex1">Contact No
                  </label>
                  <input type="text" class="form-control"  placeholder="Contact No.">
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <textarea rows="5" cols="15" class="form-control" placeholder="Instruction">
                  </textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col">
                  <label for="ex1">Upload Artwork
                  </label>
                  <input type="file" class="form-control" >
                </div>
              </div>
            </form>
            <a href="#" class="btn btn-success col">SUBMIT
            </a>
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
  </div>
</div>
<!--end Controller -->
</div>
<div class="container mt-5">
  <center>
    <h2>OUR WORK
    </h2>
    <p>We believe our work speaks for itself.
    </p>
  </center>
  <div id="imgBox">
    <div class="row mt-3">
      <div class="col-lg-4">
        <img class="img-responsive" src="https://www.qualitypatches.com/wp-content/uploads/2019/12/embroidery-800-copy2-min-400x400.jpg" >
      </div>
      <div class="col-lg-4">
        <img class="img-responsive" src="https://www.qualitypatches.com/wp-content/uploads/2019/12/embroidery-800-copy8-min-400x400.jpg"  >
      </div>
      <div class="col-lg-4">
        <img class="img-responsive" src="https://www.qualitypatches.com/wp-content/uploads/2019/12/embroidery-800-copy12-min-400x400.jpg" >
      </div>
      <div class="col-lg-4">
        <img class="img-responsive" src="https://www.qualitypatches.com/wp-content/uploads/2019/12/embroidery-800-copy-min-400x400.jpg" >
      </div>
      <div class="col-lg-4">
        <img class="img-responsive" src="https://www.qualitypatches.com/wp-content/uploads/2020/02/fish-min-400x400.jpg"  >
      </div>
      <div class="col-lg-4">
        <img class="img-responsive" src="https://www.qualitypatches.com/wp-content/uploads/2019/12/embroidery-800-copy5-min-400x400.jpg" >
      </div>
    </div>
  </div>
  <div  class="row mt-5">
    <div class="col-lg-4">
      <div class="wpb_content_element  vc_custom_1570762721083 ">
        <header id="ut_header_5f13dd34b94f8" class="section-header ut-no-title-linebreak-mobile ut-no-lead-linebreak-mobile pt-style-7 header-center header-tablet-center header-mobile-center ut-first-section-title">
          <h2 data-title="FREE ARTWORK &amp; DESIGN" class="section-title   ut-flowtyped ut-title-loaded" style="font-size: 16px;">
            <span style="color:white;">FREE ARTWORK &amp; DESIGN
            </span>
          </h2>
        </header>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="wpb_content_element  vc_custom_1570762721083">
        <header id="ut_header_5f13dd34b94f8" class="section-header ut-no-title-linebreak-mobile ut-no-lead-linebreak-mobile pt-style-7 header-center header-tablet-center header-mobile-center ut-first-section-title">
          <h2 data-title="FREE ARTWORK &amp; DESIGN" class="section-title   ut-flowtyped ut-title-loaded" style="font-size: 16px;">
            <span style="color:white;text-align: center;">FREE ARTWORK &amp; DESIGN
            </span>
          </h2>
        </header>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="wpb_content_element  vc_custom_1570762721083">
        <header id="ut_header_5f13dd34b94f8" class="section-header ut-no-title-linebreak-mobile ut-no-lead-linebreak-mobile pt-style-7 header-center header-tablet-center header-mobile-center ut-first-section-title">
          <h2 data-title="FREE ARTWORK &amp; DESIGN" class="section-title   ut-flowtyped ut-title-loaded" style="font-size: 16px;">
            <span style="color:white;text-align: center;">FREE ARTWORK &amp; DESIGN
            </span>
          </h2>
        </header>
      </div>
    </div>
  </div>
  <!--end row-->
  <div class="mt-5">
    <center>
      <h2>WELCOME TO QUALITY PATCHES
      </h2>
      <p>Your favorite digitizing company Quality Punch, Inc. brings you Quality Patches.
      </p>
    </center>
    <div class="row">
      <div class="col-lg-6">
        <h3>About Us
        </h3>
        <p style="text-align: justify">With years of award winning digitizing experience, combined with the latest patch making machines, we present Quality Patches. We give you the same dedicated customer service, with your assigned digitizers and your familiar reps – all behind your trusted name of Quality Punch.
        </p>
      </div>
      <div class="col-lg-6">
        <h3>Why Choose Us?
        </h3>
        <p style="text-align: justify"> Quality Patches we make beautiful custom patches for any purpose. Whether you are part of an organization, a member of the military, celebrating an event, remembering a loved one, or simply love patches, we can create your perfect patch. We use the finest quality threads, fabrics, and workmanship, so your patches will be durable and will look great for years to come.
        </p>
      </div>
    </div>
  </div>
</div>
<!--end container-->
<div  class="bg-contacts mt-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mt-5">
        <div class="card" style="background: rgb(216 216 216 / 94%);border: none;">
          <div class="card-body">
            <div class="ut-left-footer-area clearfix">
              <div style="color: black; padding: 15px;">
                <h3 class="vc_custom_heading" style="color: black; text-align: left;">Company Information
                </h3>
                <div class="wpb_text_column wpb_content_element " style="text-align: left;">
                  <div class="wpb_wrapper">
                    <div class="subhead">
                      <p>Your favorite digitizing company Quality Punch, Inc. brings you Quality Patches.
                      </p>
                    </div>
                    <p>With years of award winning digitizing experience, combined with the latest patch making machines, we present Quality Patches. We give you the same dedicated customer service, with your assigned digitizers and your familiar reps – all behind your trusted name of Quality Punch
                    </p>
                  </div>
                </div>
                <h3 style="color: black; text-align: left;">GET IN TOUCH
                </h3>
                <p style="text-align: left;">
                  <i class="fa fa-map-marker fa-lg">
                  </i>
                  <strong>&nbsp; Address:
                  </strong>&nbsp;438 Amapola Ave, St# 235 Torrance, CA 90501
                </p>
                <p style="text-align: left;">
                  <i class="fa fa fa-phone fa-lg">
                  </i>
                  <strong>&nbsp; Phone: 
                  </strong>310 341 7743
                </p>
                <p style="text-align: left;">
                  <i class="fa fa fa-envelope-o fa-lg">
                  </i>
                  <strong>&nbsp; Email:
                  </strong>&nbsp;design@qualitypatches.com
                </p>
              </div> 
            </div>          
          </div>
        </div>
      </div>
      <div class="col-lg-6 mt-5">
        <div class="card" style="background: rgb(216 216 216 / 94%);border: none;">
          <div class="card-body">
            <div class="ut-right-footer-area clearfix">
              <style type="text/css">body #gform_wrapper_2 .gform_body .gform_fields .gfield input[type=text],body #gform_wrapper_2 .gform_body .gform_fields .gfield input[type=email],body #gform_wrapper_2 .gform_body .gform_fields .gfield input[type=tel],body #gform_wrapper_2 .gform_body .gform_fields .gfield input[type=url],body #gform_wrapper_2 .gform_body .gform_fields .gfield input[type=password]{
                color:#ffffff;
                max-width:100%;
                border-width: 1px;
                }
                body #gform_wrapper_2 .gform_body .gform_fields .gfield textarea {
                  border-width: 1px;
                  color:#ffffff;
                }
                body #gform_wrapper_2 .gform_body .gform_fields .gfield .gfield_label {
                  font-weight: normal;
                  font-weight: bold;
                  color:#0a0a0a;
                }
                body #gform_wrapper_2 .gform_body .gform_fields .gfield .ginput_complex .ginput_full label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .ginput_complex .ginput_right label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .ginput_complex .ginput_left label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .name_first label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .name_last label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .address_line_1 label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .address_line_2 label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .address_city label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .address_state label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .address_zip label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .address_country label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .gfield_time_hour label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .gfield_time_minute label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .gfield_date_month label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .gfield_date_day label,body #gform_wrapper_2 .gform_body .gform_fields .gfield .gfield_date_year label {
                  font-weight: normal;
                  color:#0a0a0a;
                  visibility: hidden ;
                }
                /* Styling for Tablets */@media only screen and ( max-width: 800px ) and ( min-width:481px ) {
                }
                /* Styling for phones */@media only screen and ( max-width: 480px ) {
                }
                /*Option to add custom CSS */</style>
              <h3 class="gform_title" style="color: black; text-align: left;">WRITE US
              </h3>
              <p>
              </p>
              <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_2">
                <div id="gf_2" class="gform_anchor" tabindex="-1">
                </div>
                <form >
                  <div class="gform_body">
                    <ul  class="gform_fields top_label form_sublabel_below description_below">
                      <li id="field_2_4" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                        <label class="gfield_label" for="input_2_4">Name
                          <span class="gfield_required">*
                          </span>
                        </label>
                        <div class="ginput_container ginput_container_text">
                          <input name="input_4"  class="form-control" type="text" value="" class="large" tabindex="49" placeholder="Name" aria-required="true" aria-invalid="false">
                        </div>
                      </li>
                      <li id="field_2_2" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                        <label class="gfield_label" for="input_2_2">Email
                          <span class="gfield_required">*
                          </span>
                        </label>
                        <div class="ginput_container ginput_container_email">
                          <input name="input_2" id="input_2_2" class="form-control" type="email" value="" class="large" tabindex="50" placeholder="Email" aria-required="true" aria-invalid="false">
                        </div>
                      </li>
                      <li id="field_2_6" class="gfield field_sublabel_below field_description_below gfield_visibility_visible">
                        <label class="gfield_label" for="input_2_6">Phone
                        </label>
                        <div class="ginput_container ginput_container_phone">
                          <input name="input_6" id="input_2_6" class="form-control"  type="tel" value="" class="large" tabindex="51" placeholder="Phone" aria-invalid="false">
                        </div>
                      </li>
                      <li id="field_2_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible">
                        <label class="gfield_label" for="input_2_3">Message
                          <span class="gfield_required">*
                          </span>
                        </label>
                        <div class="ginput_container ginput_container_textarea">
                          <textarea name=""  class="form-control"   rows="3" cols="40">Message</textarea>
                        </div>
                      </li>
                     
                    </ul>
                  </div>
                  <div class="gform_footer top_label"> 
                    <input type="submit" id="gform_submit_button_2" class="btn btn-success" value="Submit" tabindex="53"> 
                  
                  </div>
                </form>
              </div>       
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      zoomifyc.init($('#imgBox img'));
    }
                     );
  </script>
  @stop
