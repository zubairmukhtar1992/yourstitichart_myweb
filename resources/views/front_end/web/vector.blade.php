@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{url('')}}/css/font-awesome.min.css">
<script src="https://use.fontawesome.com/125b744392.js"></script>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script src="{{url('')}}/assets/files/js/vector.js"></script>
<style>
.panel-group-one,.panel-group-two,.panel-group-three,.panel-group-four,.panel-group-five,.panel-group-six{
  display: none;
}

</style>
<section class="about-bg">
  <!-- digitizing -->
  <div class="digitizing">
    <div class="container">
      <h1>
      </h1>
      <h1>VECTOR ART
      </h1>
      <br>
    </div>
  </div>
  <!-- digitizing -->
</section>
<section class="vector-carousel">
  <div class="container">
    <div class="owl-carousel owl-theme">
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
      <div class="item">
        <img src="https://www.qualitypunch.com/assets/images/vector-image12.jpg" class="img-fluid">
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="circle">
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="qp_circles">
        <div class="qp_text">
          <h4  >
            Simple ($12)
          </h4>
          <span  class="circle_one" ><i class="fa fa-plus" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="qp_circles">
        <div class="qp_text">
          <h4>Simple ($12)
          </h4>
          <span  class="circle_two" ><i class="fa fa-plus" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="qp_circles">
        <div class="qp_text">
          <h4>Simple ($12)
          </h4>
          <span  class="circle_three" ><i class="fa fa-plus" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="qp_circles">
        <div class="qp_text">
          <h4>Simple ($12)
          </h4>
          <span  class="circle_four" ><i class="fa fa-plus" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="qp_circles">
        <div class="qp_text">
          <h4>Simple ($12)
          </h4>
          <span  class="circle_five" ><i class="fa fa-plus" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="qp_circles">
        <div class="qp_text">
          <h4>Simple ($12)
          </h4>
          <span  class="circle_six" ><i class="fa fa-plus" aria-hidden="true"></i></span>
        </div>
      </div>
    </div>
  
   
  </div>
  </div>
</div>
     <div class="container">
<div class="panel-group-one mt-5 ">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 style="font-size: 20px;">
        Simple one ($12)
      </h2>
      <span class="icon" >
        <i class="fa fa-minus" aria-hidden="true"></i>
      </span>
    </div>
    <div id="collapse1" class="panel-collapse show">
      <div class="panel-body">
        <div class="container">
        <div class="row">
        <div class="col-md-4 mt-3">
          <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-1.jpg" style="height:227px;width:341px;border-width:0px;">
        </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-3.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
    </div>
        </div>
        <br>
    </div><!--end body-->
    </div> <!--panel collapse-->
    
  </div>
</div><!--panel-group-->
<div class="panel-group-two mt-5 ">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 style="font-size: 20px;">
        Simple two ($12)
      </h2>
      <span class="icon" >
        <i class="fa fa-minus" aria-hidden="true"></i>

      </span>
    </div>
    <div id="collapse1" class="panel-collapse show">
      <div class="panel-body">
        <div class="container">
        <div class="row">
        <div class="col-md-4 mt-3">
          <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-1.jpg" style="height:227px;width:341px;border-width:0px;">
        </div>
       <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-1.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-1.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
    </div>
        </div>
        <br>
    </div><!--end body-->
    </div> <!--panel collapse-->
    
  </div>
</div><!--panel-group-->

<div class="panel-group-three mt-5 ">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 style="font-size: 20px;">
        Simple three ($12)
      </h2>
      <span class="icon" >
        <i class="fa fa-minus" aria-hidden="true"></i>

      </span>
    </div>
    <div id="collapse1" class="panel-collapse show">
      <div class="panel-body">
        <div class="container">
        <div class="row">
        <div class="col-md-4 mt-3">
          <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
        </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
    </div>
        </div>
        <br>
    </div><!--end body-->
    </div> <!--panel collapse-->
    
  </div>
</div><!--panel-group-->
<div class="panel-group-four mt-5 ">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 style="font-size: 20px;">
        Simple four ($12)
      </h2>
      <span class="icon" >
        <i class="fa fa-minus" aria-hidden="true"></i>

      </span>
    </div>
    <div id="collapse1" class="panel-collapse show">
      <div class="panel-body">
        <div class="container">
        <div class="row">
        <div class="col-md-4 mt-3">
          <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-3.jpg" style="height:227px;width:341px;border-width:0px;">
        </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-3.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-3.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
    </div>
        </div>
        <br>
    </div><!--end body-->
    </div> <!--panel collapse-->
    
  </div>
</div><!--panel-group-->
<div class="panel-group-five mt-5 ">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 style="font-size: 20px;">
        Simple five ($12)
      </h2>
      <span class="icon" >
        <i class="fa fa-minus" aria-hidden="true"></i>

      </span>
    </div>
    <div id="collapse1" class="panel-collapse show">
      <div class="panel-body">
        <div class="container">
        <div class="row">
        <div class="col-md-4 mt-3">
          <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-1.jpg" style="height:227px;width:341px;border-width:0px;">
        </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-3.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
    </div>
        </div>
        <br>
    </div><!--end body-->
    </div> <!--panel collapse-->
    
  </div>
</div><!--panel-group-->
<div class="panel-group-six mt-5 ">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 style="font-size: 20px;">
        Simple six ($12)
      </h2>
      <span class="icon" >
        <i class="fa fa-minus" aria-hidden="true"></i>

      </span>
    </div>
    <div id="collapse1" class="panel-collapse show">
      <div class="panel-body">
        <div class="container">
        <div class="row">
        <div class="col-md-4 mt-3">
          <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
        </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
      <div class="col-md-4 mt-3">
        <img  id="ctl00_ContentPlaceHolderSiteContent_SimpleImage1" alt="" class="img-responsive" src="https://www.qualitypunch.com/assets/images/vector-simple-2.jpg" style="height:227px;width:341px;border-width:0px;">
      </div>
    </div>
        </div>
        <br>
    </div><!--end body-->
    </div> <!--panel collapse-->
    
  </div>
</div><!--panel-group-->
</div><!--end container-->
<div class="container">
    
  <div class="row mt-5">
    <div class="col-md-3">
      <div class="qp_bgc">
        <div class="qp_top">
          <div class="qp_circle">
          </div>
        </div>
        <h3>Lorem ipsum Lorem ipsum
        </h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
        </p>
      </div>
    </div>
    <!--end col 3-->
    <div class="col-md-3">
      <div class="qp_bgc">
        <div class="qp_top">
          <div class="qp_circle">
          </div>
        </div>
        <h3>Lorem ipsum Lorem ipsum
        </h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
        </p>
      </div>
    </div>
    <!--end col 3-->
    <div class="col-md-3">
      <div class="qp_bgc">
        <div class="qp_top">
          <div class="qp_circle">
          </div>
        </div>
        <h3>Lorem ipsum Lorem ipsum
        </h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
        </p>
      </div>
    </div>
    <div class="col-md-3">
      <div class="qp_bgc">
        <div class="qp_top">
          <div class="qp_circle">
          </div>
        </div>
        <h3>Lorem ipsum Lorem ipsum
        </h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
        </p>
      </div>
    </div>
  </div>
</div>
<!--end container-->
@stop
