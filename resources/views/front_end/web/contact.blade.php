@extends('layouts.app')
@section('content')


<section >
      <div class="about-bg">
      <div class="digitizing">
       <div class="container">
            <center><h2>Contact Us</h2></center>
       </div>
        
        
      </div>
    </div>
    </section>
    <br>    

    <div class="container">
   <div class="row">
   
    <div class="col-lg-6 mt-5">
        <h1>Contact info</h1>

        <ul class="mt-5">
            <table>
              <tr>

            <li class="mt-5"><p>
             <td> <span><b>Address </b></span> </td>   <td>  :  438 Amapola Ave, St# 235 Torrance CA 90501</td>
            </p>
           </li>
         </tr>
           <tr >

            <li><p>
             <td> <span><b>Phone</b></span> </td>   <td>:   310-909-8948 & 1888-727-5782</td>
            </p>
           </li>
         </tr>
         <tr>

            <li><p>
             <td> <span><b>Fax:</b></span> </td>   <td>: 310-347-4205</td>
            </p>
           </li>
         </tr>

         <tr>

            <li><p>
             <td> <span><b>Email:</b></span> </td>   <td>:  digitize@qualitypunch.com</td>
            </p>
           </li>
         </tr>
      </table>
        </ul>
        



    </div>

    
    <div class="col-lg-6 mt-5">
    
        <h1>Get in Touch with us</h1>
        
        <form>
            <div class="row  mt-5">
              <div class="col">
                  <label>Full Name</label>
                <input type="text" class="form-control"  placeholder="Enter name" name="name">
              </div>
              <br>
              <div class="col">
                <label>Company Name</label>
                <input type="text" class="form-control" placeholder="Enter Company Name">
              </div>
              
            </div>
            <br>
            <div class="row mt-4">
                <div class="col">
                    <label>Phone Number</label>
                  <input type="text" class="form-control"  placeholder="Enter Phone Number" name="Phone">
                </div>
                <div class="col">
                  <label>Email</label>
                  <input type="email" class="form-control" placeholder="Enter Email">
                </div>
                
              </div>
              <div class="row mt-5">
                <div class="col">
                    <label>Question/Query: 
                    </label>
                 <textarea rows="4" cols="50" class="form-control"> </textarea>
                </div>
               
                
              </div>
              <div class="mt-3">
              <input type="submit"  value="Contact Us" id="contact"  class="btn btn-link">
            </div>
          </form>
        
    </div>





   </div>
    </div>
    
<br><br>
      <!--Map -->
	<section>

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3619.7666491886935!2d67.36062621507081!3d24.871818250927387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb333709d26dcd9%3A0x3740ac5b17152351!2sDezinesoft!5e0!3m2!1sen!2s!4v1585308524962!5m2!1sen!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </section>
	





@stop