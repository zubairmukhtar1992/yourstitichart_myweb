@extends('layouts.app') @section('content')
<style>
	.product_bar {
    min-height: 98px;
    width: 100%;
    margin: 30px 0 25px;
    -webkit-background-size: cover;
    background-size: cover;
    float: left;
}
.bx-wrapper .bx-pager.bx-default-pager a {
    width: 30px;
    height: 4px;
    border-radius: 0;
    background: #fad65a !important;
}
</style>
<body>
<div class="container-fluid slider-bg">
<vue-modal
			image="[{'img2':'./images/big3.png','img1':'./images/small3.png'},{'img2':'./images/big2.png','img1':'./images/small2.png'},{'img2':'./images/big4.png','img1':'./images/small4.png'}]"
			interval="5000" asseturl="{{URL('')}}">
</vue-modal>
  
  </div>
    <div class="container">
    
        <div class="row">
            <div class="col-md-4">
                <div class="qp_bgc">
                    <div class="qp_top">
                        <div class="qp_circle">

                            <img src="{{ url('') }}/assets/uploads/yourstitchart/product/digitizing.png" class="img-fluid" id="images-icon">
                        </div>
                    </div>
                    <h3>Lorem ipsum Lorem ipsum</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                </div>

            </div>
            <!--end col 4-->

            <div class="col-md-4">

                <div class="qp_bgc">
                    <div class="qp_top">
                        <div class="qp_circle">

                            <img src="{{ url('') }}/assets/uploads/yourstitchart/product/vector-clipart.png" class="img-fluid" id="images-icon">

                        </div>
                    </div>

                    <h3>Lorem ipsum Lorem ipsum</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                </div>

            </div>
            <!--end col 4-->

            <div class="col-md-4">
                <div class="qp_bgc">
                    <div class="qp_top">
                        <div class="qp_circle">

                            <img src="{{ url('') }}/assets/uploads/yourstitchart/product/patchesclipart.png" class="img-fluid" id="image-patehes">

                        </div>
                    </div>

                    <h3>Lorem ipsum Lorem ipsum</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                </div>

            </div>
            <!--end col 4-->

        </div>
        <!--row-->
    </div>
    <!--container-->

    <div class="home_products">

        <section>

            <div class="container">
			<div class="product_bar" style="background: url({{url('')}}/assets/uploads/yourstitchart/product/Header1.png)  no-repeat center top" >
                    <div class="red_bar_txt">                            
                    </div>
                    <div class="white_text">                        
                        <p class="white_text_p1"><strong>Free Trial offer:</strong> We believe in building long-lasting</p>
<p class="white_text_p2">relationships with our customers, not a one-time deal. </p>
<p class="white_text_p3">So, your first order is free.</p>
                    </div>
                  
                </div>
     <!-- <img alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/Header1.png" class="img-fluid" style="border-width:0px;"> -->

                <div class="product_item home_digitizing">

                    <img alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/home-services.png" style="border-width:0px;">
                </div>
            </div>
        </section>
    </div>

    <div class="home_products">
        <!--DIGITIZING ART SERVICE-->

        <section>
            <br>
            <br>
            <div class="container">
                <img alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/Header1.png" class="img-fluid" style="border-width:0px;">
                <div class="product_item home_digitizing">

                    <img alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/Vector-image.png" style="border-width:0px;">
                </div>
            </div>
        </section>
    </div>

    <br>

    <div class="home_products">
        <!--DIGITIZING ART SERVICE-->

        <section>
            <br>
            <br>
            <div class="container">
                <img alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/Header1.png" class="img-fluid" style="border-width:0px;">
                <div class="product_item home_digitizing">

                    <img alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/Patches-image.png" style="border-width:0px;">
                </div>
                
            </div>
        </section>
    </div>

    <br>

    <section class="achievement">
        <div class="container">
            <div class="row">

                <h3>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>

            </div>

        </div>
    </section>

    <div class="achievement-bg">
        <div class="container">
            <div class="row">

                <div class="col-md-4">

                    <div class="qp_bgc">
                        <div class="qp_top">
                            <div class="qp_circle">
                                <img src="{{ url('') }}/assets/uploads/yourstitchart/product/left-chest.png" class="img-fluid" id="images-icon">

                            </div>
                        </div>

                        <h3>Lorem ipsum Lorem ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                    </div>

                </div>
                <!--end col 4-->
                <div class="col-md-4">

                    <div class="qp_bgc">
                        <div class="qp_top">
                            <div class="qp_circle">

                                <img src="{{ url('') }}/assets/uploads/yourstitchart/product/cap.png" class="img-fluid" id="images-icon">

                            </div>
                        </div>

                        <h3>Lorem ipsum Lorem ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                    </div>

                </div>
                <!--end col 4-->
                <div class="col-md-4">

                    <div class="qp_bgc">
                        <div class="qp_top">
                            <div class="qp_circle">

                                <img src="{{ url('') }}/assets/uploads/yourstitchart/product/Jacket-clipart.png" class="img-fluid" id="images-icon">

                            </div>
                        </div>

                        <h3>Lorem ipsum Lorem ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                    </div>

                </div>
                <!--end col 4-->
                <div class="col-md-4">

                    <div class="qp_bgc">
                        <div class="qp_top">
                            <div class="qp_circle">
                                <img src="{{ url('') }}/assets/uploads/yourstitchart/product/towel.png" class="img-fluid" id="images-icon">

                            </div>
                        </div>

                        <h3>Lorem ipsum Lorem ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                    </div>

                </div>
                <!--end col 4-->
                <div class="col-md-4">

                    <div class="qp_bgc">
                        <div class="qp_top">
                            <div class="qp_circle">

                                <img src="{{ url('') }}/assets/uploads/yourstitchart/product/patchesclipart.png" class="img-fluid" id="image-patehes">

                            </div>
                        </div>

                        <h3>Lorem ipsum Lorem ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                    </div>

                </div>
                <!--end col 4-->
                <div class="col-md-4">

                    <div class="qp_bgc">
                        <div class="qp_top">
                            <div class="qp_circle">

                                <img src="{{ url('') }}/assets/uploads/yourstitchart/product/vector-clipart.png" class="img-fluid" id="images-icon">

                            </div>
                        </div>

                        <h3>Lorem ipsum Lorem ipsum</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                    </div>

                </div>
                <!--end col 4-->

            </div>
        </div>
        <!--end container-->
    </div>
    <br>
    <br>
    <div class="container-fluid bussiness-bg">

        <div class="container">

            <center>
                <h3 class="p-5" style="color:white;">HOW WE PROCESS YOUR ORDER</h3></center>

            <img src="{{ url('') }}/assets/uploads/yourstitchart/product/help_your_business_img.png" class="img-fluid">

        </div>

        <br>
    </div>
    <section class="achievement">
        <div class="container">
            <div class="row">

                <h3 style="position: relative;left:18%;">DIGITIZING AWARDS AND ACHIEVEMENTS</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>

            </div>
            <br>
            <br>

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/1.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/2.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/5.JPG" class="img-fluid">

                </div>

                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/4.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/3.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/6.JPG" class="img-fluid">

                </div>

                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/7.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/8.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/9.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/10.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/11.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/12.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/13.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/14.JPG" class="img-fluid">

                </div>

                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/15.JPG" class="img-fluid">

                </div>
                <div class="item">
                    <img src="{{ url('') }}/assets/uploads/yourstitchart/product/16.JPG" class="img-fluid">

                </div>
            </div>

        </div>
    </section>

    @stop