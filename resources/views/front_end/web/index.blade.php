@extends('layouts.app') @section('content')
<style>
	.product_bar {
    min-height: 98px;
    width: 100%;
    margin: 30px 0 25px;
    -webkit-background-size: cover;
    background-size: cover;
    float: left;
}

</style>

<body>
	<div class="container-fluid  slider-bg">
		<vue-modal image="[{'img2':'./images/big3.png','img1':'./images/small3.png'},{'img2':'./images/big2.png','img1':'./images/small2.png'},{'img2':'./images/big4.png','img1':'./images/small4.png'}]" interval="6000" asseturl="{{URL('')}}"></vue-modal>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="bottom-heading">WE GIVE TOP QUALITY DESIGNS FOR EMBROIDERY,SCREEN PRINTING & PATCHES</div>
			</div>
		</div>
	</div>
	<!--end container -->
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="qp_bgc">
					<div class="qp_top">
						<div class="qp_circle">
							<img src="{{ url('') }}/uploads/yourstitchart/product/digitizing.png" class="img-fluid" id="images-icon">
						</div>
					</div>
					<h3>Lorem ipsum Lorem ipsum</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
				</div>
			</div>
			<!--end col 4-->
			<div class="col-md-4">
				<div class="qp_bgc">
					<div class="qp_top">
						<div class="qp_circle">
							<img src="{{ url('') }}/uploads/yourstitchart/product/vector-clipart.png" class="img-fluid" id="images-icon">
						</div>
					</div>
					<h3>Lorem ipsum Lorem ipsum</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
				</div>
			</div>
			<!--end col 4-->
			<div class="col-md-4">
				<div class="qp_bgc">
					<div class="qp_top">
						<div class="qp_circle">
							<img src="{{ url('') }}/uploads/yourstitchart/product/patchesclipart.png" class="img-fluid" id="image-patehes">
						</div>
					</div>
					<h3>Lorem ipsum Lorem ipsum</h3>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
				</div>
			</div>
			<!--end col 4-->
		</div>
		<!--row-->
	</div>
	<!-- end container-->
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="banner ">
					<img alt="" src="{{ url('') }}/uploads/yourstitchart/product/Header1.png" class="img-fluid" style="border-width:0px;">
				</div>
				<!-- end banner-->
				<div class="product_item home_digitizing">
					<img alt="" src="{{ url('') }}/uploads/yourstitchart/product/home-services.png" style="border-width:0px;">
				</div>
			</div>
			<!-- end col-->
		</div>
		<!-- end row-->
	</div>
	<!-- end container-->
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="banner  mt-5">
					<img alt="" src="{{ url('') }}/uploads/yourstitchart/product/Header1.png" class="img-fluid" style="border-width:0px;">
				</div>
				<!-- end banner-->
				<div class="product_item home_digitizing">
					<img alt="" src="{{ url('') }}/uploads/yourstitchart/product/Vector-image.png" style="border-width:0px;">
				</div>
			</div>
			<!-- end col-->
		</div>
		<!-- end row-->
	</div>
	<!-- end container-->
	<div class="container-fluid achievement-" style="background-image:url({{url('')}}/uploads/yourstitchart/product/achievements.jpg)">
		<div class="container">
			<div class="row">
			  <div class="p-4 text-center">
				<h2>Lorem ipsum Lorem ipsum</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
			    </div>
			</div>
		</div>
		<div class="achievement-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="qp_bgc">
							<div class="qp_top">
								<div class="qp_circle">
									<img src="{{ url('') }}/uploads/yourstitchart/product/left-chest.png" class="img-fluid" id="images-icon">
								</div>
							</div>
							<h3>Lorem ipsum Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						</div>
					</div>
					<!--end col 4-->
					<div class="col-md-4">
						<div class="qp_bgc">
							<div class="qp_top">
								<div class="qp_circle">
									<img src="{{ url('') }}/uploads/yourstitchart/product/cap.png" class="img-fluid" id="images-icon">
								</div>
							</div>
							<h3>Lorem ipsum Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						</div>
					</div>
					<!--end col 4-->
					<div class="col-md-4">
						<div class="qp_bgc">
							<div class="qp_top">
								<div class="qp_circle">
									<img src="{{ url('') }}/uploads/yourstitchart/product/Jacket-clipart.png" class="img-fluid" id="images-icon">
								</div>
							</div>
							<h3>Lorem ipsum Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						</div>
					</div>
					<!--end col 4-->
					<div class="col-md-4">
						<div class="qp_bgc">
							<div class="qp_top">
								<div class="qp_circle">
									<img src="{{ url('') }}/uploads/yourstitchart/product/towel.png" class="img-fluid" id="images-icon">
								</div>
							</div>
							<h3>Lorem ipsum Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						</div>
					</div>
					<!--end col 4-->
					<div class="col-md-4">
						<div class="qp_bgc">
							<div class="qp_top">
								<div class="qp_circle">
									<img src="{{ url('') }}/uploads/yourstitchart/product/patchesclipart.png" class="img-fluid" id="image-patehes">
								</div>
							</div>
							<h3>Lorem ipsum Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						</div>
					</div>
					<!--end col 4-->
					<div class="col-md-4">
						<div class="qp_bgc">
							<div class="qp_top">
								<div class="qp_circle">
									<img src="{{ url('') }}/uploads/yourstitchart/product/vector-clipart.png" class="img-fluid" id="images-icon">
								</div>
							</div>
							<h3>Lorem ipsum Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						</div>
					</div>
					<!--end col 4-->
				</div>
			</div>
			<!--end container-->
		</div>
	</div>
	<!---end container-->
	<section style="background-color:black;">
		<div class="container">
		<div class="text-center">
				<h2 class="p-4" style="color:white;">HOW WE PROCESS YOUR ORDER</h2>
		</div>	
			<img src="{{ url('') }}/assets/uploads/yourstitchart/product/help_your_business_img.png" class="img-fluid">
			<br>
		</div>
	</section>
	<section class="achievement">
		<div class="container">
			<div class="row">
			     <div class="text-center">
				<h2>DIGITIZING AWARDS AND ACHIEVEMENTS</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
			    </div>
			</div>
			<br>
			<br>
			<div class="owl-carousel owl-theme">
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/1.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/2.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/5.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/4.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/3.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/6.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/7.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/8.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/9.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/10.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/11.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/12.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/13.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/14.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/15.JPG" class="img-fluid">
				</div>
				<div class="item">
					<img src="{{ url('') }}/uploads/yourstitchart/product/16.JPG" class="img-fluid">
				</div>
			</div>
		</div>
	</section>@stop