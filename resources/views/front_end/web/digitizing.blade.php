   @extends('layouts.app')
@section('content')

<section class="about-bg">
      
      <div class="digitizing">
       <div class="container">
        <h1>Custom Embroidery Digitizing  </h1>
                <br>
                <p>We have won over 47 digitizing awards, and will do an outstanding job for you, guaranteed!</p>
       </div>
        
        
      </div>
     
    </section>

<br>
<br>
<br>
        <div class="digitizing_tabs">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#basics_pricing" aria-controls="home" role="tab" data-toggle="tab">
                    Basics &amp; Pricing
                </a></li>
                <li role="presentation"><a href="#digitizing_offered" aria-controls="digitizing_offered" role="tab" id="tab-two" data-toggle="tab">                
                    Digitizing Offered
                </a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="basics_pricing">
                    <!-- digitizing tabs1 -->
                    <!-- Basic Pricing -->
                    <div class="basic_pricing">

                        <div class="container">
                            <div class="product_item home_digitizing">
                                                 <img  alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/art2.png" style="border-width:0px;">

                            </div>
                            <div class="product_item home_digitizing">
                       <img  alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/art1.png" style="border-width:0px;">
                            </div>
                            <div class="product_item home_digitizing">
                             
                                 <img  alt="" src="{{ url('') }}/assets/uploads/yourstitchart/product/art.png" style="border-width:0px;">
                            </div>
                        </div>

                        <div class="container">
                            <div class="container">
                                <div class="product">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>
                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->
                                <div class="col-md-3">

                                    <div class="qp_bgc">
                                        <div class="qp_top">
                                            <div class="qp_circle"></div>
                                        </div>

                                        <h3>Lorem ipsum Lorem ipsum</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

                                    </div>

                                </div>
                                <!--end col 3-->

                            </div>
                        </div>
                        <!--end container-->

                        <section>

                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="digitizing_offered">

                <section>
                <div class="container">

                <div class="row">
                <div class="col-lg-6 mt-5">
                <h3>LEFT CHEST DESIGNS</h3>
                <hr>
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book..</p>

                 </div>
                 <div class="col-lg-6 mt-5">
                 <img src="{{ url('') }}/assets/uploads/yourstitchart/product/dummy.png" class="img-responsive float-right">
                 </div>
                 <div class="col-lg-6 mt-5">
                  <img src="{{ url('') }}/assets/uploads/yourstitchart/product/dummy.png" class="img-responsive float-left">
                  </div>
                  <div class="col-lg-6 mt-5">

                <h3>LEFT CHEST DESIGNS</h3>
                <hr>
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book..</p>

                </div>
              
                <div class="col-lg-6 mt-5">

                <h3>LEFT CHEST DESIGNS</h3>
                <hr>
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book..</p>

                </div>
               <div class="col-lg-6 mt-5">

              <img src="{{ url('') }}/assets/uploads/yourstitchart/product/dummy.png" class="img-responsive float-right">

                </div>
  
                 <div class="col-lg-6 mt-5">

              <img src="{{ url('') }}/assets/uploads/yourstitchart/product/dummy.png" class="img-responsive float-left">

                </div>
                 <div class="col-lg-6 mt-5">

                <h3>LEFT CHEST DESIGNS</h3>
                <hr>
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book..</p>

                </div>

                </div>
                 <br> <br>
               </div>
               </section>






                </div>

                </section>

                @stop