@extends('layouts.app') @section('content')
<style>
	.form-control {
	        display: block;
	        width: 100%;
	        height: calc(1.5em + .75rem + 2px);
	        padding: .375rem .75rem;
	        font-size: 1rem;
	        font-weight: 400;
	        line-height: 1.5;
	        color: #495057;
	        background-color: #ffffffab;
	        background-clip: padding-box;
	        border: 1px solid #ced4da;
	        border-radius: 7px;
	        border: none;
	        border-bottom: 2px solid #ffc107;
	        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
	    }
	    
	    .form-control:focus {
	        color: #495057;
	        background-color: #fff;
	        border-color: #060108;
	        outline: 0;
	        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0);
	    }
</style>
<section class="about-bg">
	<div class="digitizing">
		<div class="container">
			<center>
				<h2>LOG IN </h2>
			</center>
		</div>
	</div>
</section>
<div class="container-fluid signup">
	<div class="container">
		<br>
		<br>
		<br>
      
		<div class="row">
			<div class="col-md-5 offset-lg-3">
			@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
@endif
				<div class="panel-group">
					<div class="panel panel-warning">
						<div class="panel-heading signup-panel" style="color:white;font-size: 20px;">LOG IN</div>
						<div class="panel-body">
							<form   method="post" action="{{route('login.action')}}"   >
								<br>
								@csrf
								<div class="form-row">
									<div class="col">
										<label>USERNAME</label>
										<input type="text" class="form-control" placeholder="Enter Username" value="{{ old('username') }}" name="username">
										@if ($errors->has('username'))
                                         <span class="error">{{ $errors->first('username') }}</span>
                                     @endif    
									</div>
								</div>
								<!--form-row-->
								<br>
								<div class="form-row">
									<div class="col">
										<label>PASSWORD</label>
										<input type="password" class="form-control" placeholder="Enter password" value="{{ old('password') }}" name="password">
								
									  	@if ($errors->has('password'))
                                            <span class="error">{{ $errors->first('password') }}</span>
                                           @endif
									</div>
								</div>
								<!--form-row-->
								<br>
								<div class="form-group">
                                <input type="submit" name="btnsubmit" class="btn btn-success  col-md-3" id="signup-btn" value="Login">
								</div>
							</form>
							</br>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end container-->@stop