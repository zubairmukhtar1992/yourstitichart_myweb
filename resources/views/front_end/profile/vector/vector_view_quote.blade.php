@extends('layouts.profile')
@section('content')

  <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h2 class="header-title">Quote Order Record</h2>
                                        <p class="text-muted font-13 mb-4">
                                      
                                        </p>

                                                <table id="selection-datatable" class="table table-sm mb-2">
                                                    <thead>
                                                    <tr>
                                                    <th>S.N</th>
                                                    <th>Vector Quote Number</th>
                                                    <th>Quote Name</th>
                                                    <th>Received Date</th>
                                                    <th>Released Date</th>
                                                    <th>Price</th>
                                                    <th>Order Details</th>
                </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($vectorquotes   as $key =>$vectorquote)
                                                    
                                                    <tr>
                                                      <td></td>
                                                      <td>VQN-0{{$vectorquote->id}}</td>
                                                      <td>{{$vectorquote->quote_name}}</td>
                                                      <td>{{$vectorquote->received_date}}</td>
                                                      <td>{{$vectorquote->released_date ?? 'Procssing' }}</td>
                                                      <td>$0:00</td>
                                                      <td><a href="{{route('vector.show',$vectorquote->id)}}">View </a> |
                                                      <a href="{{route('vector.quote.edit',$vectorquote->id)}}">Edit</a>
                                                      </td
                                                      </tr>
                                                       @endforeach
                                                       </tbody>
                                                       </table>
                                            
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                </div>
                                <!-- end row-->

                      

                   
@stop