@extends('layouts.profile') @section('content')

<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <center>
                    <h2>Place Vector Order</h2></center>

                <form action="{{route('vector.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-3">
                        <label>Vector Name</label>
                        <input type="text" class="form-control"  name="vector_name"    value="{{old('vector_name')}}"   placeholder="Vector Name" name="vector_name" >
                       
                    </div>

                    <div class="form-group mb-3">
                        <label>Height (inches)</label>
                        <input type="text" class="form-control"   value="{{old('height')}}"     placeholder="Height (inches)"   name="height" >
                       
                    </div>

                    <div class="form-group mb-3">
                        <label>Width (inches)</label>
                        <input type="text" class="form-control"    value="{{old('width')}}"    placeholder="Width (inches)"  name="width"  >
                       
                    </div>
                    <div class="form-group mb-3">
                        <label>Color Type</label>
                        <select class="form-control"   name="color"  >
                            <option value="">Color Type</option>
                            <option value="Pantone Matching System">Pantone Matching System</option>
                            <option value="Three Color Process">Three Color Process</option>
                            <option value="Four Color Process">Four Color Process</option>
                        </select>
                     
                    </div>
                    <div class="form-group mb-3">
                        <label>Order Type</label>
                        <select class="form-control"   name="order" >
                            <option value="">Order Type</option>
                            <option value="Screen Printing">Screen Printing</option>
                            <option value="DTG Printing">DTG Printing</option>
                            <option value="Silk Screening">Silk Screening</option>
                            <option value="Vinly Cutting">Vinly Cutting</option>
                            <option value="Sublimtaion">Sublimtaion</option>
                            <option value="Photo Touchup">Photo Touchup</option>

                        </select>
                      
                    </div>
                    <div class="form-group mb-3">
                        <label >Order Fabric</label>
                        <select class="form-control"   name="order_fabric" >
                            <option value="">Required Format</option>
                            <option value="Coral Draw (CDR)">Coral Draw (CDR)</option>
                            <option value="Adobe Illustrator (AI)">Adobe Illustrator (AI)</option>
                            <option value="Encapsulated Post Script (EPS)">Encapsulated Post Script (EPS)</option>
                            <option value="Adobe Photoshop (PLT)">Adobe Photoshop (PLT)</option>
                            <option value="Adobe Acrobat (PDF)">Adobe Acrobat (PDF)</option>
                            <option value="Macromedia Freehand (FH)">Macromedia Freehand (FH)</option>
                            <option value="Others">Others</option>

                        </select>
                       
                    </div>
                    <div class="form-group mb-3">
                        <label>Vector Instruction</label>
                        <textarea row="40" column="40" name="instruction"  class="form-control"></textarea>
                       
                    </div>
                    <div class="form-group mb-3">
                        <label>Upload ArtWork</label>
                        <input type="file" name="image"  class="form-control">
                     
                    </div>
                    <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-3" type="submit">Place Order</button>
                </form>

            </div>
            <!-- end card-body-->
        </div>
        <!-- end card-->
    </div>
    <!-- end col-->

</div>
<!-- end row -->

@stop