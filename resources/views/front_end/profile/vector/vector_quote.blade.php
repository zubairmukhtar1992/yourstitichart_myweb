@extends('layouts.profile')
@section('content')

                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <center><h2 >Place Vector Quote</h2></center>

                                        <form    action="{{route('vector.store')}}"  method="post"  enctype="multipart/form-data"   >
                                            @csrf
                                            <div class="form-group mb-3">
                                                <label>Quote Name</label>
                                                <input type="text" class="form-control"  placeholder="Enter Quote Name" value="{{old('quote_name')}}"    name="quote_name">
                                                @if ($errors->has('quote_name'))
                                              <span class="error">{{ $errors->first('quote_name') }}</span>
                                              @endif
                                              </div>
                                             <div class="form-group mb-3">
                                            <label>Instruction</label>
                                             <textarea row="40" column="40" name="instruction" value="{{old('instruction')}}" placeholder="Enter Instruction"  class="form-control"></textarea>
                                             @if ($errors->has('instruction'))
                                              <span class="error">{{ $errors->first('instruction') }}</span>
                                              @endif
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Upload ArtWork</label>
                                                <input type="file" class="form-control" name="image">
                                                @if ($errors->has('image'))
                                              <span class="error">{{ $errors->first('image') }}</span>
                                              @endif
                                              </div>
                                            
                                            <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-3" type="submit">Vector Quote</button>
                                        </form>

                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->


                        </div>
                        <!-- end row -->
                           

                   
@stop