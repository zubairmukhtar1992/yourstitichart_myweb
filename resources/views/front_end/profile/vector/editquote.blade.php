@extends('layouts.profile')
@section('content')

                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <center><h2 >Place Vector Quote</h2></center>

                                        <form    action="{{route('vector.quote.update',$editquote->id)}}"  method="post"  enctype="multipart/form-data"   >
                                            @csrf
                                            <div class="form-group mb-3">
                                                <label>Quote Name</label>
                                                <input type="text" class="form-control"  placeholder="Enter Quote Name" value="{{$editquote->quote_name}}"    name="quote_name">
                                               
                                              </div>
                                             <div class="form-group mb-3">
                                            <label>Instruction</label>
                                             <textarea row="40" column="40" name="instruction" placeholder="Enter Instruction"  class="form-control">{{$editquote->instruction}}</textarea>
                                            
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Upload ArtWork</label>
                                                <input type="file" class="form-control" name="image">
                                              
                                              </div>
                                            
                                            <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-3" type="submit">Update Quote</button>
                                        </form>

                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->


                        </div>
                        <!-- end row -->
                           

                   
@stop