@extends('layouts.profile')
@section('content')

  <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h2 class="header-title">Vector Order Record</h2>
                                        <p class="text-muted font-13 mb-4">
                                      
                                        </p>

                                                <table id="selection-datatable" class="table table-sm mb-2">
                                                    <thead>
                                                    <tr>
                                                    <th >S.N</th>
                                                    <th >Vector Order Number</th>
                                                    <th >Vector Name</th>
                                                    <th >Received Date</th>
                                                    <th >Released Date</th>
                                                    <th >Price</th>
                                                    <th >Order Details</th>
                </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($vectororders    as $key =>$vectororder)
                                                    
                                                    <tr>
                                                      <td></td>
                                                      <td>VON-0{{$vectororder->id}}</td>
                                                      <td>{{$vectororder->vector_name}}</td>
                                                      <td>{{$vectororder->recieved_date}}</td>
                                                      <td>--</td>
                                                      <td>$0:00</td>
                                                      <td><a href="{{route('vector.show',$vectororder->id)}}">View </a> |
                                                      <a href="{{route('vector.edit',$vectororder->id)}}">Edit</a>
                                                      </td
                                                      </tr>
                                                       @endforeach
                                                       </tbody>
                                                       </table>
                                            
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                </div>
                                <!-- end row-->

                      

                   
@stop