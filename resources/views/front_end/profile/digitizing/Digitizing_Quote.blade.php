@extends('layouts.profile') @section('content')

<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <center>
                    <h2>Place Digitizing Quote</h2></center>

                <form    action="{{route('digitizing.quote.store')}}"  method="post"  enctype="multipart/form-data"  >
                    @csrf
                    <div class="form-group mb-3">
                        <label >Quote Name</label>
                        <input type="text" class="form-control" name="quote_name"  placeholder="Quote Name" >
                    
                        @if ($errors->has('quote_name'))
                        <span class="error">{{ $errors->first('quote_name') }}</span>
                         @endif
                         
                    </div>
                   
                    <div class="form-group mb-3">
                        <label >Height (inches)</label>
                        <input type="text" class="form-control" name="height"  placeholder="Height (inches)" >
                        @if ($errors->has('height'))
                        <span class="error">{{ $errors->first('height') }}</span>
                         @endif
                    </div>
                   
                    <div class="form-group mb-3">
                        <label >Width (inches)</label>
                        <input type="text" class="form-control" name="width"  placeholder="Height (inches)" >
                        @if ($errors->has('width'))
                        <span class="error">{{ $errors->first('width') }}</span>
                         @endif
                    </div>
                   
                    <div class="form-group mb-3">
                        <label >Quote Placement</label>
                        <select  class="form-control"  name="quote_placement"  >
                            <option value="">Quote Placement</option>
                            <option value="Cap Front">Cap Front</option>
                            <option value="Cap Side">Cap Side</option>
                            <option value="Cap Back">Cap Back</option>
                            <option value="Low Profile Cap">Low Profile Cap</option>
                            <option value="Left Chest">Left Chest</option>
                            <option value="Right Chest">Right Chest</option>
                            <option value="Front Pocket">Front Pocket</option>
                            <option value="Full Front">Full Front</option>
                            <option value="Jacket Back">Jacket Back</option>
                            <option value="Cap/Chest">Cap/Chest</option>
                            <option value="Knit Caps">Knit Caps</option>
                            <option value="Beanie Caps">Beanie Caps</option>
                            <option value="Visor">Visor</option>
                            <option value="Sleeve">Sleeve</option>
                            <option value="Patches">Patches</option>
                            <option value="Apron">Apron</option>
                            <option value="Applique Design">Applique Design</option>
                            <option value="Bags">Bags</option>
                            <option value="Towel">Towel</option>
                            <option value="Gloves">Gloves</option>
                            <option value="Blankets">Blankets</option>
                            <option value="Sweatshirt">Sweatshirt</option>
                            <option value="Hoodie">Hoodie</option>
                            <option value="Wrist Band">Wrist Band</option>
                            <option value="Seat Cover">Seat Cover</option>
                            <option value="Quilt">Quilt</option>

                        </select>
                        @if ($errors->has('quote_placement'))
                        <span class="error">{{ $errors->first('quote_placement') }}</span>
                         @endif
                    </div>
                   
                    <div class="form-group mb-3">
                        <label >Order Fabric</label>
                        <select  class="form-control"   name="order_fabric"   > 
                            <option value="">Quote Fabric</option>
                            <option value="Twill">Twill</option>
                            <option value="Pique">Pique</option>
                            <option value="Polyester/Performance">Polyester/Performance</option>
                            <option value="Cotton">Cotton</option>
                            <option value="Fleece">Fleece</option>
                            <option value="Towel">Towel</option>
                            <option value="Leather">Leather</option>
                            <option value="Denim">Denim</option>
                            <option value="Silk">Silk</option>
                            <option value="Nylon">Nylon</option>
                            <option value="Quilt">Quilt</option>
                            <option value="Canvas">Canvas</option>
                            <option value="Single Jersey">Single Jersey</option>
                            <option value="Wool">Wool</option>
                            <option value="Velvet">Velvet</option>
                            <option value="Apron">Apron</option>
                            <option value="Blanket">Blanket</option>
                            <option value="Polar Fleece">Polar Fleece</option>
                            <option value="Stretchy Polyester/Light Knit">Stretchy Polyester/Light Knit </option>
                            <option value="Knit">Knit</option>
                            <option value="Others">Others</option>
                        </select>
                        @if ($errors->has('order_fabric'))
                        <span class="error">{{ $errors->first('order_fabric') }}</span>
                         @endif
                    </div>
                   
                    <div class="form-group mb-3">
                        <label >Order Instruction</label>
                        <textarea row="40" column="40" name="instruction" class="form-control"></textarea>
                        @if ($errors->has('instruction'))
                        <span class="error">{{ $errors->first('instruction') }}</span>
                         @endif
                    </div>
                
                    <div class="form-group mb-3">
                        <label >Upload ArtWork</label>
                        <input type="file" name="image"  class="form-control">
                        @if ($errors->has('image'))
                        <span class="error">{{ $errors->first('image') }}</span>
                         @endif
                    </div>
                  
                    
                    <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-3" type="submit">Send Quote</button>
                </form>

            </div>
            <!-- end card-body-->
        </div>
        <!-- end card-->
    </div>
    <!-- end col-->

</div>
<!-- end row -->

@stop