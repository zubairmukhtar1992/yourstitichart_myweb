<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> YourStitchart Customer Profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->



    <!-- App css -->
    <link href="{{ url('') }}/assets/files/dist/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ url('') }}/assets/files/dist/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('') }}/assets/files/dist/css/app.min.css" rel="stylesheet" type="text/css" />

</head>
<style>
    .navbar-custom {
        background-color: #020202;
        -webkit-box-shadow: 0 0 35px 0 rgba(154, 161, 171, .15);
        box-shadow: 0 0 35px 0 rgba(154, 161, 171, .15);
        padding: 0 10px 0 0;
        position: fixed;
        left: 0;
        right: 0;
        height: 70px;
        z-index: 100;
    }

    .left-side-menu {
        width: 240px;
        background: #FAD65A;
        bottom: 0;
        padding: 20px 0;
        position: fixed;
        -webkit-transition: all .2s ease-out;
        transition: all .2s ease-out;
        top: 70px;
        -webkit-box-shadow: 0 0 35px 0 rgba(154, 161, 171, .15);
        box-shadow: 0 0 35px 0 rgba(154, 161, 171, .15);
    }

    #sidebar-menu>ul>li>a:active,
    #sidebar-menu>ul>li>a:focus,
    #sidebar-menu>ul>li>a:hover {
        color: #ffffff;
        text-decoration: none;
        background-color: #020202;
    }

    .nav-second-level li a:focus,
    .nav-second-level li a:hover,
    .nav-thrid-level li a:focus,
    .nav-thrid-level li a:hover {
        color: #ffffff;
        background-color: black;
    }

    #sidebar-menu>ul>li>a {
        color: #020202;
        display: block;
        padding: 13px 20px;
        position: relative;
        -webkit-transition: all .4s;
        transition: all .4s;
        font-family: "Cerebri Sans,sans-serif";
        font-size: 18px;
    }

    .nav-second-level li a,
    .nav-thrid-level li a {
        padding: 7px 23px 10px 58px;
        color: #020202;
        display: block;
        position: relative;
        -webkit-transition: all .4s;
        transition: all .4s;
        font-size: .875rem;
    }

    #sidebar-menu>ul>li>ul {
        padding-left: 0px;
    }

</style>

<body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        <div class="navbar-custom">
            <ul class="list-unstyled topnav-menu float-right mb-0">




                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown"
                        href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <span class="pro-user-name ml-1">
                            Zubair <i class="mdi mdi-chevron-down"></i>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome !</h6>
                        </div>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="fe-user"></i>
                            <span>Settings</span>
                        </a>





                        <div class="dropdown-divider"></div>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="fe-log-out"></i>
                            <span>Logout</span>
                        </a>

                    </div>
                </li>




            </ul>

            <!-- LOGO -->
            <div class="logo-box">
                <a href="index.html" class="logo text-center">
                    <span class="logo-lg">
                        <img src="{{ url('') }}/assets/files/dist/images/logo-light.png" alt=""
                            height="18" style="
    height: 50px;">
                        <!-- <span class="logo-lg-text-light">UBold</span> -->
                    </span>

                </a>
            </div>

            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect waves-light">
                        <i class="fe-menu"></i>
                    </button>
                </li>



            </ul>
        </div>
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">

            <div class="slimscroll-menu">

                <!--- Sidemenu -->
                <div id="sidebar-menu">

                    <ul class="metismenu" id="side-menu">

                        <li class="menu-title">WELLCOME ZUBAIR</li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-airplay"></i>

                                <span> Dashboards </span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">

                            </ul>
                        </li>


                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-airplay"></i>

                                <span> DIGITIZING </span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="{{ url('/place_order') }}">Place Order </a>
                                </li>
                                <li>
                                    <a href="#">View Orders Record </a>
                                </li>
                                <li>
                                    <a href="#">Get Quote </a>
                                </li>
                                <li>
                                    <a href="#"> View Quotes Record </a>
                                </li>

                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-airplay"></i>

                                <span> VECTOR </span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="#">Place Order </a>
                                </li>
                                <li>
                                    <a href="#">View Orders Record </a>
                                </li>
                                <li>
                                    <a href="#">Get Quote </a>
                                </li>
                                <li>
                                    <a href="#"> View Quotes Record </a>
                                </li>

                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-airplay"></i>

                                <span>ORDERS </span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="#">Pending Orders </a>
                                </li>
                                <li>
                                    <a href="#">Invoices </a>
                                </li>


                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);">
                                <i class="fe-airplay"></i>

                                <span> Setting </span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="#">My Profile </a>
                                </li>
                                <li>
                                    <a href="#">Change Password</a>
                                </li>


                            </ul>
                        </li>



                    </ul>

                </div>
                <!-- End Sidebar -->

                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                        <li class="breadcrumb-item active">Profile</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->


                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-9">
                            <div class="card">
                                <div class="card-body">
                                    <h2>Place Digitizing Order</h2>

                                    <form class="needs-validation" novalidate>
                                        <div class="form-group mb-3">
                                            <label for="validationCustom01">Order Name</label>
                                            <input type="text" class="form-control" id="validationCustom01"
                                                placeholder="Order Name" required>

                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="validationCustom02">Purchase Order Number</label>
                                            <input type="text" class="form-control" id="validationCustom02"
                                                placeholder="Purchase Order Number" required>

                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="validationCustom02">Height (inches)</label>
                                            <input type="text" class="form-control" id="validationCustom02"
                                                placeholder="Height (inches)" required>

                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="validationCustom02">Width (inches)</label>
                                            <input type="text" class="form-control" id="validationCustom02"
                                                placeholder="Height (inches)" required>

                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="validationCustom02">Order Placment</label>
                                            <select class="form-control" id="validationCustom04" required>
                                                <option value="">Select Order Placement</option>
                                                <option>Alaska</option>
                                                <option>Hawaii</option>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="validationCustom02">Order Fabric</label>
                                            <select class="form-control" id="validationCustom04" required>
                                                <option value="">Select Order Fabric</option>
                                                <option>Alaska</option>
                                                <option>Hawaii</option>
                                            </select>
                                        </div>

                                        <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-3"
                                            type="submit">Submit form</button>
                                    </form>

                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div> <!-- end col-->


                    </div>
                    <!-- end row -->


                </div> <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{ url('') }}/assets/files/dist/js/vendor.min.js"></script>

        <!-- Chart JS -->
        <script src="{{ url('') }}/assets/files/dist/libs/chart-js/Chart.bundle.min.js"></script>

        <script src="{{ url('') }}/assets/files/dist/libs/moment/moment.min.js"></script>
        <script src="{{ url('') }}/assets/files/dist/libs/jquery-scrollto/jquery.scrollTo.min.js">
        </script>
        <script src="{{ url('') }}/assets/files/dist/libs/sweetalert2/sweetalert2.min.js"></script>

        <!-- Chat app -->
        <script src="{{ url('') }}/assets/files/dist/js/pages/jquery.chat.js"></script>

        <!-- Todo app -->
        <script src="{{ url('') }}/assets/files/dist/js/pages/jquery.todo.js"></script>

        <!-- Dashboard init JS -->
        <script src="{{ url('') }}/assets/files/dist/js/pages/dashboard-3.init.js"></script>

        <!-- App js-->
        <script src="{{ url('') }}/assets/files/dist/js/app.min.js"></script>

</body>

</html>
