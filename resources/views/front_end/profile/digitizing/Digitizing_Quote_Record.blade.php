@extends('layouts.profile') @section('content')
<style>
.design {
    background: #d9d9e2;
    border-bottom: 1px solid #929eaa69;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <a  href="{{url('Digitizing/Quote')}}" class="btn btn-success btn-rounded waves-effect waves-light col-lg-1 float-right" >back</a>

                <h2>View Digitizing Quote</h2>

                <table class="table">
                    <thead>
                        <tr>
                            <td width="21%" class="design second-border">Order No</td>
                            <td width="25%" class="second-border">DQN-0{{$digitizing_qoutes->id}}</td>
                            <!--
      <td colspan="2" class="invtblhd">Sent Back </td>
      -->
                        </tr>
                    </thead>
                    <tbody>
                      
                        <tr>
                            <td  class="design"  >Qoute Name</td>
                            <td>{{$digitizing_qoutes->quote_name}}</td>
                        </tr>
                        <tr>
                            <td class="design">Date</td>
                            <td>{{$digitizing_qoutes->created_at}}</td>

                        </tr>
                        <tr>
                            <td class="design"   >Height</td>
                            <td>{{$digitizing_qoutes->height}}</td>

                        </tr>
                        <tr>
                            <td  class="design"  >Width</td>
                            <td>{{$digitizing_qoutes->width}}</td>

                        </tr>

                        <tr>
                            <td  class="design"  >Qoute Placement</td>
                            <td>{{$digitizing_qoutes->quote_placement}}</td>
                        </tr>

                        <tr>
                            <td  class="design"  >Order Fabric</td>
                            <td>{{$digitizing_qoutes->order_fabric}}</td>
                        </tr>
                      

                        </tr>

                        <tr>
                            <td class="design"   >Order Instructions</td>
                            <td style="height:50px;">{{$digitizing_qoutes->instruction}}</td>
                        </tr>
                        <tr>
                            <td  class="design"   >Your Artwork</td>
                            <td>
                                <table width="300" border="0" cellpadding="0" cellspacing="0">

                                    <tbody>
                                        <tr>
                                        <img width="50%" src="{{config('yourstitchart.file_url').$digitizing_qoutes->image}}">                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2>Order Specification </h2>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td  class="design"  >Height</td>
                            <td>
                                <table>
                                </table>

                            </td>

                        </tr>
                        <tr>
                            <td  class="design"   >Width</td>
                            <td>

                                <table>
                                    <tbody>
                                        <tr>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>

                        </tr>
                        <tr>
                            <td class="design"   >Stitches</td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td  class="design"   >Order Price : </td>
                            <td>$ 0.00 USD</td>
                        </tr>
                        <tr>

                        </tr>

            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
<!-- end row-->

@stop