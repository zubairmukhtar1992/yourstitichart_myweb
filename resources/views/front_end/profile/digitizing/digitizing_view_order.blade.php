@extends('layouts.profile')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">

        <h2 class="header-title">Digitizing Order Record</h2>
        <p class="text-muted font-13 mb-4">

        </p>

        <table id="selection-datatable" class="table table-sm mb-2">
          <thead>
            <tr>
              <th>S.N </th>
              <th>Order No</th>
              <th>Order Name</th>
              <th>Received Date</th>
              <th>Released Date</th>
              <th>Price</th>
              <th>Status</th>
              <th>Order Details</th>


            </tr>


          </thead>
          <tbody>
            <?php
                                              $counter=0;
                                            ?>
            @foreach($digitizings as $key =>$digitizing)
            <tr>
              <?php   $counter=$counter+1;  ?>
              <td>{{$counter}}</td>
              <td>DPO-0{{$digitizing->id}}</td>
              <td>{{$digitizing->order_name}}</td>
              <td>{{$digitizing->created_at}}</td>
              <td>{{$digitizing->released_date ??  '-'}}</td>
              <td>$0:00</td>
              <td>
                @php
                $val=["Pending","InProgress","Complete","Canceled"]
                @endphp
                {{$val[$digitizing->status-1]}}
              </td>
              <td><a href="{{url('/Digitizing/Record',$digitizing->id)}}">View</a> |
                <a href="{{route('digitizing.edit',$digitizing->id)}}">Edit</a>
              </td>

            </tr>
            @endforeach

          </tbody>


        </table>
        <br>
        </br>

      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>
<!-- end row-->




@stop
