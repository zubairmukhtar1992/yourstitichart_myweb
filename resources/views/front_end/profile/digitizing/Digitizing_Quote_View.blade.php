@extends('layouts.profile') @section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h2 class="header-title">Digitizing Quote Records
</h2>
                <p class="text-muted font-13 mb-4">

                </p>

                <table id="selection-datatable" class="table table-sm mb-2">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Quote No</th>
                            <th>Quote Name</th>
                            <th>Received Date</th>
                            <th>Released Dates</th>
                            <th>Price</th>
                            <th>Convert To Order</th>
                            <th>Quote Details</th>
                        </tr>
                    </thead>
                    <tbody>
                @foreach($digitizingquotes   as   $key => $digitizingquote)
                <tr>
                <td> </td>
                <td>DQN-0{{$digitizingquote->id}}</td>
                <td>{{$digitizingquote->quote_name}}</td>
                <td>{{$digitizingquote->received_date}}</td>
                <td>{{$digitizingquote->released_date  ??   'processing'}}</td>
                <td>$0:00</td>
                <td> </td>
                <td><a href="{{route('digitizing.qoute.show',$digitizingquote->id)}}">View | </a> 
                <a href="{{route('digitizing.qoute.edit',$digitizingquote->id)}}">Edit</a>
                 </td>
                </tr> 
                @endforeach
                    </tbody>
                </table>

            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
<!-- end row-->

@stop