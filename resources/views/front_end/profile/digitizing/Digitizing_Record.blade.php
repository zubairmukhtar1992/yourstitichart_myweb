@extends('layouts.profile') @section('content')
<style>
.design {
    background: #d9d9e2;
    border-bottom: 1px solid #929eaa69;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <a  href="{{url('Digitizing/View')}}" class="btn btn-success btn-rounded waves-effect waves-light col-lg-1 float-right" >back</a>

                <h2>View Digitizing Order</h2>
                     
                <table class="table">
                    <thead>
                        <tr>
                            <td width="21%" class="design second-border">Order No</td>
                            <td width="25%" class="second-border">DPO-0{{$digitizing_view->id}}</td>
                          
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  class="design"  >Order Name</td>
                            <td>{{$digitizing_view->order_name}}</td>
                        </tr>
                        <tr>
                            <td class="design">Date</td>
                            <td>{{$digitizing_view->created_at}}</td>

                        </tr>
                        <tr>
                            <td class="design"   >Height</td>
                            <td>{{$digitizing_view->height}}</td>

                        </tr>
                        <tr>
                            <td  class="design"  >Width</td>
                            <td>{{$digitizing_view->width}}</td>

                        </tr>

                        <tr>
                            <td  class="design"  >Order Placement</td>
                            <td>{{$digitizing_view->order_placement}}</td>
                        </tr>

                        <tr>
                            <td  class="design"  >Order Fabric</td>
                            <td>{{$digitizing_view->order_fabric}}</td>
                        </tr>
                        <tr>
                            <td  class="design" rowspan="2"><span class="form_left_text2">Order Format</span></td>
                            <td rowspan="2">{{$digitizing_view->required_format}}</td>

                        </tr>

                        <tr>

                        </tr>

                        <tr>
                            <td class="design"   >Order Instructions</td>
                            <td style="height:50px;">{{$digitizing_view->instruction}}</td>
                        </tr>
                        <tr>
                            <td  class="design"   >Your Artwork</td>
                            <td>
                                <table width="300" border="0" cellpadding="0" cellspacing="0">

                                    
                                        <tr>
                                        <td><img width="50%" src="{{config('yourstitchart.file_url').$digitizing_view->image}}"></td> 
                                        </tr>
                                    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2>Order Specification </h2>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td  class="design"  >Height</td>
                            <td>
                                <table>
                                </table>

                            </td>

                        </tr>
                        <tr>
                            <td  class="design"   >Width</td>
                            <td>

                                <table>
                                    <tbody>
                                        <tr>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>

                        </tr>
                        <tr>
                            <td class="design"   >Stitches</td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td  class="design"   >Order Price : </td>
                            <td>$ 0.00 USD</td>
                        </tr>
                        <tr>

                        </tr>

            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
<!-- end row-->

@stop