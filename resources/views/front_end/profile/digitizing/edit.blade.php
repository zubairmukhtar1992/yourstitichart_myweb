@extends('layouts.profile') @section('content')
<style>

</style>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
            <a  href="{{url('Digitizing/View')}}" class="btn btn-success btn-rounded waves-effect waves-light col-lg-2 float-right" >back</a>

                <center>
                    <h2>Update Digitizing Order</h2></center>

                <form  method="post" action="{{route('digitizing.update',$digitizing_edit->id)}}"   enctype="multipart/form-data" >
                   @csrf
                  
                    <div class="form-group mb-4">
                        <label >Order Name</label>
                        <input type="text" class="form-control"  placeholder="Order Name" value="{{$digitizing_edit->order_name}}"  name="order_name"   >
                       
                    </div>
                   
                    <div class="form-group mb-4">
                        <label >Height (inches)</label>
                        <input type="text" class="form-control" placeholder="Height (inches)" name="height" value="{{$digitizing_edit->height}}" >
                        
                    </div>

                    <div class="form-group mb-4">
                        <label >Width (inches)</label>
                        <input type="text" class="form-control"  placeholder="Height (inches)" name="width" value="{{$digitizing_edit->width}}" >
                       
                    </div>
                    <div class="form-group mb-4">
                        <label>Order Placement</label>
                        <select class="form-control"  name="order_placement"  >
                            <option   value="{{$digitizing_edit->order_placement}}">{{$digitizing_edit->order_placement}}</option>
                            <option   value="Cap Front" >Cap Front</option>
                            <option   value="Cap Side"  >Cap Side</option>
                            <option   value="Cap Back"  >Cap Back</option>
                            <option  value="Low Profile Cap"   >Low Profile Cap</option>
                            <option  value="Left Chest"   >Left Chest</option>

                        </select>
                      
                    </div>
                    <div class="form-group mb-4">
                        <label >Required Format</label>
                        <select class="form-control"  name="required_format"  >
                        <option value="{{$digitizing_edit->required_format}}" >{{$digitizing_edit->required_format}}</option>
	            		<option value="Tajima Machine File (.DST)">Tajima Machine File (.DST)</option>
		            	<option value="Barudan Machine File (.DSB)">Barudan Machine File (.DSB)</option>
                        <option value="Janome Machine File (.JEF)">Janome Machine File (.JEF)</option>
                        <option value="Compucon Machine File (.XXX)">Compucon Machine File (.XXX)</option>
                        <option value="Happy Machine File (.TAP)">Happy Machine File (.TAP)</option>
                        <option value="Toyota Machine File (.100)">Toyota Machine File (.100)</option>
                        <option value=".EMB/.DST">.EMB/.DST</option>
                        <option value=".PES/.DST">.PES/.DST</option>
                        <option value=".EXP/.DST">.EXP/.DST</option>
                        <option value=".CND/.DST">.CND/.DST</option>
                        <option value=".OFM/.DST">.OFM/.DST</option>
                        <option value=".PXF/.DST">.PXF/.DST</option>
                        </select>
                      
                    </div>
                    <div class="form-group mb-4">
                        <label >Order Fabric</label>
                        <select class="form-control"   name="order_fabric">
                            <option >{{$digitizing_edit->order_fabric}}</option>
                            <option value="Twill">Twill</option>
                            <option value="Pique">Pique</option>
                            <option value="Polyester/Performance">Polyester/Performance</option>
                            <option value="Cotton">Cotton</option>
                            <option value="Fleece">Fleece</option>
                            <option value="Towel">Towel</option>
                            <option value="Leather">Leather</option>
                            <option value="Denim">Denim</option>
                            <option value="Silk">Silk</option>
                            <option value="Nylon">Nylon</option>
                            <option value="Quilt">Quilt</option>
                            <option value="Canvas">Canvas</option>
                            <option value="Single Jersey">Single Jersey</option>
                            <option value="Wool">Wool</option>
                            <option value="Velvet">Velvet</option>
                            <option value="Apron">Apron</option>
                            <option value="Blanket">Blanket</option>
                            <option value="Polar Fleece">Polar Fleece</option>
                            <option value="Stretchy Polyester/Light Knit ">Stretchy Polyester/Light Knit </option>
                            <option value="Knit">Knit</option>
                            <option value="Others">Others</option>

                        </select>
                        @if ($errors->has('order_fabric'))
                        <span class="error">{{ $errors->first('order_fabric') }}</span>
                         @endif
                    </div>
                    <div class="form-group mb-4">
                        <label >Order Instruction</label>
                        <textarea row="40" column="40"  class="form-control" name="instruction">{{$digitizing_edit->instruction}}</textarea>
                        @if ($errors->has('instruction'))
                        <span class="error">{{ $errors->first('instruction') }}</span>
                         @endif
                    </div>

                    <div class="form-group mb-4">
                        <label >Upload ArtWork</label>
                        <input type="file" name="image"  class="form-control">
                      
                    </div>
                    <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-3" type="submit">Update Order</button>
                </form>

            </div>
            <!-- end card-body-->
        </div>
        <!-- end card-->
    </div>
    <!-- end col-->

</div>
<!-- end row -->

@stop