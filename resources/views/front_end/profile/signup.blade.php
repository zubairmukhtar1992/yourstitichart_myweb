@extends('layouts.app') @section('content')
<link href="https://parsleyjs.org/src/parsley.css" rel="stylesheet" type="text/css" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<style>
.error_form
{
    color:red;
}

.form-control {
    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #ffffffab;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 7px;
    border: none;
    border-bottom: 2px solid #ffc107;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.form-control:focus {
    color: #495057;
    background-color: #fff;
    border-color: #060108;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0);
}
</style>
<section class="about-bg">

    <div class="digitizing">
        <div class="container">
            <center>
                <h2>Sign Up Now ! </h2></center>
        </div>
    </div>
</section>

<div class="container-fluid signup">
    <div class="container">
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-group">
                    <div class="panel panel-warning">
                        <div class="panel-heading signup-panel" style="color:white;font-size: 20px;margin-left: 0px;"><span class="number">1</span> Username & Password</div>
                        <div class="panel-body">
                            <form  id="demo-form"  action="{{route('signup.store')}}" method="post" data-parsley-validate >
                              @csrf
                                <br>
                                <div class="form-row">
                                    <div class="col">
                                        <label>USERNAME</label>
                                        <input type="text" id="username" class="form-control" placeholder="Enter Username" value="{{ old('username') }}" name="username"  required  >
                                        @if ($errors->has('username'))
                                         <span class="error">{{ $errors->first('username') }}</span>
                                          @endif
                                    </div>
                                    <div class="col">
                                        <label>PASSWORD</label>
                                        <input type="password"  class="form-control" placeholder="Enter password" value="{{ old('current_password') }}"   name="current_password" required >
                                        @if ($errors->has('current_password'))
                                         <span class="error">{{ $errors->first('current_password') }}</span>
                                          @endif
                                    </div>
                                </div>
                                <!--form-row-->
                                <br>
                                <div class="form-row">

                                    <div class="col-md-6">
                                        <label>Confirm Password</label>
                                        <input type="password"  class="form-control" placeholder="Enter confirm password"  value="{{ old('confirm_password') }}" name="confirm_password" required>
                                        @if ($errors->has('confirm_password'))
                                         <span class="error">{{ $errors->first('confirm_password') }}</span>
                                          @endif
                                    </div>
                                </div>
                                <!--form-row-->
                                <br>
                                <div class="panel-heading signup-panel" style="color:white;font-size: 20px;margin-left: 0px;"> <span class="number">2</span> Contact Information</div>
                                <br>
                                <div class="form-row">
                                    <div class="col">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" id="Fname" placeholder="Enter First Name" value="{{ old('first_name') }}"  name="first_name"  required>
                                        @if ($errors->has('first_name'))
                                         <span class="error">{{ $errors->first('first_name') }}</span>
                                     @endif
                                    </div>
                                    <div class="col">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" id="Lname" placeholder="Enter Last Name" value="{{ old('last_name') }}" name="last_name"  required>
                                        @if ($errors->has('last_name'))
                                         <span class="error">{{ $errors->first('last_name') }}</span>
                                     @endif
                                    </div>
                                </div>
                                <!--form-row-->
                                <br>
                                <div class="form-row">
                                    <div class="col">
                                        <label>Company Name</label>
                                        <input type="text" class="form-control" id="Cname" placeholder="Enter Company Name" value="{{ old('company_name') }}" name="company_name"  required>
                                        @if ($errors->has('company_name'))
                                         <span class="error">{{ $errors->first('company_name') }}</span>
                                     @endif
                                    </div>
                                    <div class="col">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" id="Phone" placeholder="Enter Phone" value="{{ old('phone') }}" name="phone"  required>
                                        @if ($errors->has('phone'))
                                         <span class="error">{{ $errors->first('phone') }}</span>
                                     @endif
                                    </div>
                                    <br>

                                </div>
                                <!--form-row-->
                                <br>
                                <div class="form-row">
                                    <div class="col">
                                        <label>Email</label>
                                        <input type="email" class="form-control" id="form_email" placeholder="Enter Email" value="{{ old('email') }}" name="email" required>
                                        <span class="error_form" id="email_error_message"></span>
                                        @if ($errors->has('email'))
                                         <span class="error">{{ $errors->first('email') }}</span>
                                     @endif
                                       </div>
                                        <div class="col">
                                        <label>Country</label>
                                        <select class="form-control" name="Country"  required>
                                            <option value="{{ old('Country') }}" >Select Country</option>
                                            <option value="UnitedStates">United States</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                            <option value="Zimbabwe">France</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="South Africa">South Africa</option>
                                        </select>
                                        @if ($errors->has('Country'))
                                         <span class="error">{{ $errors->first('Country') }}</span>
                                     @endif
                                    </div>

                                </div>
                                <!--form-row-->
                                <br>

                                <div class="form-group">

                                    <input type="submit" name="btnsubmit" class="btn btn-success  col-md-2" id="signup-btn" value="Register Now">
                                </div>

                            </form>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="https://parsleyjs.org/dist/parsley.min.js"></script>
<script class="example">
    $(function () {
      $('#demo-form').parsley().on('field:validated', function() {
        var ok = $('.parsley-error').length === 0;
        $('.bs-callout-info').toggleClass('hidden', !ok);
        $('.bs-callout-warning').toggleClass('hidden', ok);
      })
      .on('form:submit', function() {
        return false; // Don't submit form for this demo
      });
    });
    </script>

@stop
