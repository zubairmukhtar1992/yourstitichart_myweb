@extends('layouts.profile') @section('content')
<style>
    .design {
        background: #d9d9e2;
        border-bottom: 1px solid #929eaa69;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <a href="{{url('Patches/View')}}" class="btn btn-primary btn-rounded waves-effect waves-light col-lg-1 float-right">back</a>

                <h2>Patches Order</h2>

                <table class="table">
                    <thead>
                        <tr>
                            <td width="21%" class="design second-border">Order No</td>
                            <td width="25%" class="second-border">PON-0{{$patchesrecords->id}}</td>

                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td class="design">Width</td>
                            <td>{{$patchesrecords->Width}}</td>
                        </tr>
                        <tr>
                            <td class="design">Height</td>
                            <td>{{$patchesrecords->Height}}</td>

                        </tr>

                        </tr>

                        <tr>
                            <td class="design">Embroidered patches</td>
                            <td style="height:50px;">{{$patchesrecords->Embroidered}}</td>
                        </tr>
                        <tr>
                            <td class="design">Iron on / Heat Seal</td>
                            <td style="height:50px;">{{$patchesrecords->Iron}}</td>
                        </tr>
                        <tr>
                            <td class="design">Quantity</td>
                            <td style="height:50px;">{{$patchesrecords->Quantity}}</td>
                        </tr>
                        <tr>
                            <td class="design">Date Required</td>
                            <td style="height:50px;">{{$patchesrecords->Received_date}}</td>
                        </tr>
                        <tr>
                            <td class="design">Instruction</td>
                            <td style="height:50px;">{{$patchesrecords->Instruction}}</td>
                        </tr>
                        <tr>
                            <td class="design">Your Artwork</td>
                            <td>
                                <table width="300" border="0" cellpadding="0" cellspacing="0">

                                    <tbody>
                                        <tr>
                                            <td><img width="50%" src="{{config('yourstitchart.file_url').$patchesrecords->image}}"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2>Order Specification </h2>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                           
                        </tr>
                   
                        <tr>
                            <td class="design">Width </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td class="design">Height </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="design">Order Price : </td>
                            <td>$ 0.00 USD</td>
                        </tr>
                        <tr>

                        </tr>

            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
<!-- end row-->

@stop