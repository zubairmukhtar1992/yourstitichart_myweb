@extends('layouts.profile')
@section('content')

                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <center><h2>Update Patches Quote</h2></center>

                                        <form    action="{{route('Patches.quote.update',$patchequoteedit->id)}}"   method="post" enctype="multipart/form-data"  >
                                              @csrf
                                            <div class="form-group mb-3">
                                                <label>Width</label>
                                                <input type="text" class="form-control"  name="width" value="{{$patchequoteedit->width}}"  placeholder="width (inches)"   >
                                               
                                            </div>
                                            
                                            <div class="form-group mb-3">
                                                <label>Height</label>
                                                <input type="text" class="form-control" name="height" value="{{$patchequoteedit->height}}"  placeholder="Height (inches)" >
                                               
                                            </div>
                                        
                                            <div class="form-group mb-3">
                                                <label>Embroidered patches</label>
                                                <select   class="form-control"   name="embroidered"     >
                                                <option  value="{{$patchequoteedit->Embroidered}}">Embroidered patches</option>
                                                <option value="Embroidered Patches">Embroidered Patches</option>
                                                <option value="Chenille Patches">Chenille Patches</option>
                                                <option value="Leather Patches">Leather Patches</option>
                                                <option value="PVC / Rubber Patches">PVC / Rubber Patches</option>
                                                <option value="Printed / Sublimated Patches">Printed / Sublimated Patches</option>
                                                <option value="Woven Patches">Woven Patches</option>
                                                <option value="Embroidered Keychains">Embroidered Keychains</option>
                                                <option value="Chenille Keychains">Chenille Keychains</option>
                                                <option value="Leather Keychains">Leather Keychains</option>
                                                <option value="PVC / Rubber Keychains">PVC / Rubber Keychains</option>
                                                <option value="Printed / Sublimated Keychains">Printed / Sublimated Keychains</option>
                                                <option value="Woven Keychains">Woven Keychains</option>
                                                </select>
                                              
                                            </div>
                                            <div class="form-group mb-3">
                                            <label>Iron on / Heat Seal</label>
                                               <select   class="form-control"     name="iron"   >
                                               <option value="{{$patchequoteedit->Iron}}" >Iron on / Heat Seal</option>
                                               <option value="Velcro (Both Hook &amp; Loop)">Velcro (Both Hook &amp; Loop)</option>
                                               <option value="Peel &amp; Stick / Self Adhesive">Peel &amp; Stick / Self Adhesive</option>
                                               <option value="Sew on">Sew on</option>
                                               </select>
                                             
                                            </div>
                                          
                                            <div class="form-group mb-3">
                                                <label>Quantity</label>
                                                <input type="number" class="form-control"   name="quantity" value="{{$patchequoteedit->Quantity}}"    placeholder="Quantity"  >
                                             
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Date Required</label>
                                                <input type="date" class="form-control"  name="date" value="{{$patchequoteedit->Date}}"  placeholder="Date"  >
                                              
                                            </div>
                                             <div class="form-group mb-3">
                                            <label>Vector Instruction</label>
                                             <textarea row="40" column="40" name="instruction"  class="form-control">{{$patchequoteedit->Instruction}}</textarea>
                                           
                                            </div>
                                             <div class="form-group mb-3">
                                             <label>Upload Artwork</label>
                                          <input type="file" name="image"   class="form-control"> 
                                         
                                            </div>
                                            <button class="btn btn-success waves-effect waves-light  col-lg-2" type="submit"><i class="mdi mdi-content-save"></i> Update</button>
                                        </form>

                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->


                        </div>
                        <!-- end row -->
                           

                   
@stop