@extends('layouts.profile') @section('content')
<style>
    .design {
        background: #d9d9e2;
        border-bottom: 1px solid #929eaa69;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <a href="{{url('Patches/Quote')}}" class="btn btn-primary btn-rounded waves-effect waves-light col-lg-1 float-right">back</a>

                <h2>Patches Quote Order</h2>

                <table class="table">
                    <thead>
                        <tr>
                            <td width="21%" class="design second-border">Order No</td>
                            <td width="25%" class="second-border">PQN-0{{$patchequoterecord->id}}</td>

                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td class="design">Width</td>
                            <td>{{$patchequoterecord->width}}</td>
                        </tr>
                        <tr>
                            <td class="design">Height</td>
                            <td>{{$patchequoterecord->height}}</td>

                        </tr>

                        </tr>

                        <tr>
                            <td class="design">Embroidered patches</td>
                            <td style="height:50px;">{{$patchequoterecord->Embroidered}}</td>
                        </tr>
                        <tr>
                            <td class="design">Iron on / Heat Seal</td>
                            <td style="height:50px;">{{$patchequoterecord->Iron}}</td>
                        </tr>
                        <tr>
                            <td class="design">Quantity</td>
                            <td style="height:50px;">{{$patchequoterecord->Quantity}}</td>
                        </tr>
                        <tr>
                            <td class="design">Date Required</td>
                            <td style="height:50px;">{{$patchequoterecord->Received_date}}</td>
                        </tr>
                        <tr>
                            <td class="design">Instruction</td>
                            <td style="height:50px;">{{$patchequoterecord->Instruction}}</td>
                        </tr>
                        <tr>
                            <td class="design">Your Artwork</td>
                            <td>
                                <table width="300" border="0" cellpadding="0" cellspacing="0">

                                    <tbody>
                                        <tr>
                                            <td><img width="50%" src="{{config('yourstitchart.file_url').$patchequoterecord->image}}"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2>Order Specification </h2>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                           
                        </tr>
                   
                        <tr>
                            <td class="design">Width </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td class="design">Height </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="design">Order Price : </td>
                            <td>$ 0.00 USD</td>
                        </tr>
                        <tr>

                        </tr>

            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
<!-- end row-->

@stop