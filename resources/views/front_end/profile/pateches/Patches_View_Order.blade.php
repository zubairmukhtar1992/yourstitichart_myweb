@extends('layouts.profile')
@section('content')

  <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h2 class="header-title">Patches Order Record</h2>
                                        <p class="text-muted font-13 mb-4">
                                      
                                        </p>

                                                <table id="selection-datatable" class="table table-sm mb-2">
                                                    <thead>
                                                    <tr>
                                                    <th>S.N</th>
                                                    <th>Patches Order Number</th>
                                                    <th>Embroidered patches</th>
                                                    <th>Recieved Date</th>
                                                    <th>Released Date</th>
                                                    <th>price</th>
                                                    <th>Action</th>
                                                   </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($patches as $key=>$patch)
                                                    <tr>
                                                    <td></td>
                                                    <td>PON-0{{$patch->id}}</td>
                                                    <td>{{$patch->Embroidered}}</td>
                                                    <td>{{$patch->Received_date}}</td>
                                                    <td>{{$patch->released_date ??  'proccessing'}}</td>
                                                    <td>$0:00</td>
                                                    <td>
                                                    <a href="{{route('patches.show',$patch->id)}}">View</a> |
                                                     <a href="{{route('Patches.edit',$patch->id)}}">Edit</a>
                                                    </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                </div>
                                <!-- end row-->

                      

                   
@stop