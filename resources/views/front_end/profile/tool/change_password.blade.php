@extends('layouts.profile') @section('content')

<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <center>
                    <h2>Change Password</h2></center>

                <form  action="{{route('password.store')}}"   method="post"   >
                    @csrf
                    <div class="form-group mb-3">
                        <label >Current Password</label>
                        <input type="password" class="form-control" name="current_password"  placeholder="Current Password" >
                        @if ($errors->has('current_password'))
                        <span class="error">{{ $errors->first('current_password') }}</span>
                         @endif
                    </div>
                    <div class="form-group mb-3">
                        <label >New Password</label>
                        <input type="password" class="form-control"   name="new_password"  placeholder="New Password" >
                        @if ($errors->has('new_password'))
                        <span class="error">{{ $errors->first('new_password') }}</span>
                         @endif
                    </div>
                    <div class="form-group mb-3">
                        <label >Confirm Password</label>
                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" >
                        @if ($errors->has('confirm_password'))
                        <span class="error">{{ $errors->first('confirm_password') }}</span>
                         @endif
                    </div>

                    <button class="btn btn-success btn-rounded waves-effect waves-light col-lg-4" type="submit"><i class="mdi mdi-content-save"></i> Change Password</button>
                </form>

            </div>
            <!-- end card-body-->
        </div>
        <!-- end card-->
    </div>
    <!-- end col-->

</div>
<!-- end row -->

@stop