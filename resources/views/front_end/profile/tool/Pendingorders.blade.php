@extends('layouts.profile')
@section('content')

  <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body ">
                                    <h2 style="text-align:center;position: relative;right: 85px;">Processing Orders & Quotes.</h2>

                                        
                                        <div class="form-row ">
                                        <div class="select-order">
                                        <div class="col">
                                        <select class="form-control">
                                        @foreach($Pendingorders as $key=>$Pendingorder)
                                        <option >{{$Pendingorder->orders}}</option>

                                        @endforeach
                                        </select>
                                        </div>
                                        </div>

                                        <div class="col  col-md-10">
                                        <form>
                                        <div class="form-group float-right">
                                        <input type="text" name="search"  placeholder="Search"  class="form-control"   style="width: 357px;">
                                        </div>
                                        </form>
                                        </div>
                                        </div>
                                        </div>
                                       

                                        <table id="selection-datatable" class="table table-sm mb-2">
                                            <thead>
                                                <tr>
                                                    <th>S.N	</th>
                                                    <th>Order No</th>
                                                    <th>Design Name	</th>
                                                    <th>Order Type</th>
                                                    <th>Received Date</th>
                                                    <th>Released Date	</th>
                                                    <th>price</th>

                                                    <th>Order Details</th>

                                                  </tr>
                                                  
                                               
                                            </thead>
                                            <tbody>
                                            @foreach($Digitizingorders as $key=> $Digitizingorder)
                                            <tr>
                                            <td></td>
                                            <td>DPO-0{{$Digitizingorder->id}}</td>
                                            <td>{{$Digitizingorder->order_name}}</td>
                                            <td></td>
                                            <td>{{$Digitizingorder->recieved_date}}</td>
                                            <td>{{$Digitizingorder->released_date ?? 'processing'}}</td>
                                            <td>$0.00</td>
                                            </tr>
                                           @endforeach
                                           @foreach($Vectororders as $key=> $Vectororder)
                                            <tr>
                                            <td></td>
                                            <td>VON-0{{$Vectororder->id}}</td>
                                            <td>{{$Vectororder->vector_name}}</td>
                                            <td></td>
                                            <td>{{$Vectororder->recieved_date}}</td>
                                            <td>{{$Digitizingorder->released_date ?? 'processing'}}</td>
                                            <td>$0.00</td>
                                            </tr>
                                           @endforeach
                                            </tbody>
                                             
                                              
                                        </table>
                                        <br>
                                        </br>
                                        <div class="paginate float-right">
                                      
                                        </div>
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->

                      

                   
@stop